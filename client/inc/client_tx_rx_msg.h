#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cJSON.h"
#include "unistd.h"
#include "string.h"

int tx_message_to_server(int, cJSON*);
cJSON* rx_message_from_server(int sock);
