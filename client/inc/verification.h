#include "cJSON.h"
#include <unistd.h>
#include <string.h>

int verify_id(int sock, cJSON* send);
int verify_customer_selection(const char s);
int verify_admin_selection(const char s);
int verify_employee_selection(const char s);
