#include "client_tx_rx_msg.h"
#include <errno.h>

#define MAX_RECV_LEN 1024

int tx_message_to_server(int sock, cJSON* root){

    char* json_str = NULL;
    char* tx = NULL;
    int ret = 0;

    json_str = cJSON_PrintBuffered(root, 1, 1);
    int json_str_len = strlen(json_str);

    tx = malloc(json_str_len + 1);
    memset(tx, 0, json_str_len);
    memcpy(tx, json_str, json_str_len);

    printf("%s\n", tx);
    int tx_len = write(sock, tx, json_str_len);
    if(tx_len != json_str_len){
        printf("error write\n");
        ret = 1;
    }

    cJSON_free(json_str);
    free(tx);

    return ret;
}

cJSON* rx_message_from_server(int sock){

    char rx_buf[MAX_RECV_LEN] = {0};
    memset(rx_buf, 0, MAX_RECV_LEN);

    // bug: need to consider if msg len > MAX_RECV_LEN
    int rx_len = read(sock, rx_buf, MAX_RECV_LEN);

    if(rx_len < 0){
        printf("%s", strerror(errno));
        return NULL;
    }

    return cJSON_Parse(rx_buf);
}
