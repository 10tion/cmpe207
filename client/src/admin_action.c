#include <stdlib.h>
#include "admin_action.h"
#include "msg_typedef.h"

static void process_create_employee_account(int sock, cJSON* root);
static void process_delete_employee_account(int sock, cJSON* root);
static void process_change_employee_pw(int sock, cJSON* root);
static void process_change_admin_pw(int sock, cJSON* root);
static void process_exit(int sock, cJSON* root);
#ifdef DEBUG
static int print_preallocated(cJSON *root);
#endif
void admin_action(int sock, char s, cJSON* root){

    switch(s){
        case 'a':
            process_create_employee_account(sock, root);
            break;
        case 'b':
            process_delete_employee_account(sock, root);
            break;
        case 'c':
            process_change_employee_pw(sock, root);
            break;
        case 'd':
            process_change_admin_pw(sock, root);
            break;
        case 'e':
            process_exit(sock, root);
            break;
    }
}

/* Create a bunch of objects as demonstration. */
#ifdef DEBUG
static int print_preallocated(cJSON *root)
{
    /* declarations */
    char *out = NULL;
    char *buf = NULL;
    char *buf_fail = NULL;
    size_t len = 0;
    size_t len_fail = 0;

    /* formatted print */
    out = cJSON_Print(root);

    /* create buffer to succeed */
    /* the extra 5 bytes are because of inaccuracies when reserving memory */
    len = strlen(out) + 5;
    buf = (char*)malloc(len);
    if (buf == NULL)
    {
        printf("Failed to allocate memory.\n");
        exit(1);
    }

    /* create buffer to fail */
    len_fail = strlen(out);
    buf_fail = (char*)malloc(len_fail);
    if (buf_fail == NULL)
    {
        printf("Failed to allocate memory.\n");
        exit(1);
    }

        /* Print to buffer */
    if (!cJSON_PrintPreallocated(root, buf, (int)len, 1)) {
        printf("cJSON_PrintPreallocated failed!\n");
        if (strcmp(out, buf) != 0) {
            printf("cJSON_PrintPreallocated not the same as cJSON_Print!\n");
            printf("cJSON_Print result:\n%s\n", out);
            printf("cJSON_PrintPreallocated result:\n%s\n", buf);
        }
        free(out);
        free(buf_fail);
        free(buf);
        return -1;
    }

    /* success */
    printf("%s\n", buf);

    /* force it to fail */
    if (cJSON_PrintPreallocated(root, buf_fail, (int)len_fail, 1)) {
        printf("cJSON_PrintPreallocated failed to show error with insufficient memory!\n");
        printf("cJSON_Print result:\n%s\n", out);
        printf("cJSON_PrintPreallocated result:\n%s\n", buf_fail);
        free(out);
        free(buf_fail);
        free(buf);
        return -1;
    }

    free(out);
    free(buf_fail);
    free(buf);
    return 0;
}
#endif

static void process_create_employee_account(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)C_EMPLOYEE_ACCOUNT);

    char name[128] = {0};
    printf("input name: ");
    fgets(name, sizeof(name), stdin);

    //remove newline
    char* pos = strchr(name, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "teller name", cJSON_CreateString(name));

    char pw[128] = {0};
    printf("input password: ");
    fgets(pw, sizeof(pw), stdin);

    //remove newline
    pos = strchr(pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "password", cJSON_CreateString(pw));
#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "teller name");
    cJSON_DetachItemFromObject(root, "password");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_delete_employee_account(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)D_EMPLOYEE_ACCOUNT);

    char name[128] = {0};
    printf("input name: ");
    fgets(name, sizeof(name), stdin);

    //remove newline
    char* pos = strchr(name, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "teller name", cJSON_CreateString(name));

#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "teller name");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_change_employee_pw(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)MODIFYPW);

    char name[128] = {0};
    printf("input name: ");
    fgets(name, sizeof(name), stdin);

    //remove newline
    char* pos = strchr(name, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "teller name", cJSON_CreateString(name));

    char new_pw[128] = {0};
    printf("input new password: ");
    fgets(new_pw, sizeof(new_pw), stdin);

    //remove newline
    pos = strchr(new_pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "new password", cJSON_CreateString(new_pw));
#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "teller name");
    cJSON_DetachItemFromObject(root, "new password");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_change_admin_pw(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)MODIFY_ADMIN_PW);

    char new_pw[128] = {0};
    printf("input new password: ");
    fgets(new_pw, sizeof(new_pw), stdin);

    //remove newline
    char* pos = strchr(new_pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "new password", cJSON_CreateString(new_pw));

#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "new password");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_exit(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)EXIT);

#ifdef DEBUG
    print_preallocated(root);
#endif
    // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

