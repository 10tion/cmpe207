#include <stdlib.h>
#include "customer_action.h"
#include "msg_typedef.h"

static void process_withdraw(int sock, cJSON* root);
static void process_deposit(int sock, cJSON* root);
static void process_transfer(int sock, cJSON* root);
static void process_check_balance(int sock, cJSON* root);
static void process_trans_history(int sock, cJSON* root);
static void process_change_pw(int sock, cJSON* root);
static void process_exit(int sock, cJSON* root);
#ifdef DEBUG
static int print_preallocated(cJSON *root);
#endif
void customer_action(int sock, char s, cJSON* root){

    switch(s){
        case 'a':
            process_withdraw(sock, root);
            break;
        case 'b':
            process_deposit(sock, root);
            break;
        case 'c':
            process_transfer(sock, root);
            break;
        case 'd':
            process_check_balance(sock, root);
            break;
        case 'e':
            process_trans_history(sock, root);
            break;
        case 'f':
            process_change_pw(sock, root);
            break;
        case 'g':
            process_exit(sock, root);
            break;
    }
}

static void process_withdraw(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)WITHDRAW);

    char from[128] = {0};
    printf("input from account name: ");
    fgets(from, sizeof(from), stdin);

    //remove newline
    char* pos = strchr(from, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "account", cJSON_CreateString(from));

    double money = 0.0;
    printf("How much ? ");
    scanf("%lf", &money);
    //eat up new line
    getchar();
    // !!!! validation for money
    cJSON_AddNumberToObject(root, "amount", money);
#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "account");
    cJSON_DetachItemFromObject(root, "amount");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_deposit(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)DEPOSIT);

    char from[128] = {0};
    printf("input from account name: ");
    fgets(from, sizeof(from), stdin);

    //remove newline
    char* pos = strchr(from, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "account", cJSON_CreateString(from));

    double money = 0.0;
    printf("How much ? ");
    scanf("%lf", &money);
    //eat up new line
    getchar();

    cJSON_AddNumberToObject(root, "amount", money);
#ifdef DEBUG
    print_preallocated(root);
#endif
    // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "account");
    cJSON_DetachItemFromObject(root, "amount");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_transfer(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)TRANSFER);

    char from[128] = {0};
    printf("input from account name: ");
    fgets(from, sizeof(from), stdin);

    //remove newline
    char* pos = strchr(from, '\n');
    if(pos != NULL) *pos = '\0';
    cJSON_AddItemToObject(root, "account", cJSON_CreateString(from));

    char to[128] = {0};
    printf("input to account name: ");
    fgets(to, sizeof(to), stdin);

    //remove newline
    pos = strchr(to, '\n');
    if(pos != NULL) *pos = '\0';
    cJSON_AddItemToObject(root, "transfer account name", cJSON_CreateString(to));

    double money = 0.0;
    printf("How much ? ");
    scanf("%lf", &money);

    //eat up new line
    getchar();

    cJSON_AddNumberToObject(root, "amount", money);
#ifdef DEBUG
    print_preallocated(root);
#endif

    // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "account");
    cJSON_DetachItemFromObject(root, "transfer account name");
    cJSON_DetachItemFromObject(root, "amount");
    cJSON_DetachItemFromObject(root, "action");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_check_balance(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)BALANCE);

    char from[128] = {0};
    printf("input from account name: ");
    fgets(from, sizeof(from), stdin);

    //remove newline
    char* pos = strchr(from, '\n');
    if(pos != NULL) *pos = '\0';
    cJSON_AddItemToObject(root, "account", cJSON_CreateString(from));

     // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "account");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                cJSON* balance = cJSON_GetObjectItemCaseSensitive(rxJSON, "message");
                printf("%s\n", balance->valuestring);
#if 0
                if(cJSON_IsNumber(balance)){
                    double b = (int)balance->valuedouble;
                    printf("balance is:%f\n", b);
                }
#endif
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

/*
 * {
 *      "result":  0/1
 *      "transaction log": [
 *          {
 *              "time"   : string
 *              "balance": double
 *              "action" : string
 *          },
 *          {
 *              "time"   : string
 *              "balance": double
 *              "action" : string
 *          }
 *      ]
 * }
 *
 * */
static void process_trans_history(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)HISTORY);

    char from[128] = {0};
    printf("input from account name: ");
    fgets(from, sizeof(from), stdin);

    //remove newline
    char* pos = strchr(from, '\n');
    if(pos != NULL) *pos = '\0';
    cJSON_AddItemToObject(root, "account", cJSON_CreateString(from));

    int entries = 0;
    printf("how many entries: ");
    scanf("%d", &entries);

    //eat up new line
    getchar();

    cJSON_AddNumberToObject(root, "num_of_entries", entries);

#ifdef DEBUG
    print_preallocated(root);
#endif
    // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "account");
    cJSON_DetachItemFromObject(root, "num_of_entries");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);

    if(rxJSON != NULL){
        int entry_len = 0;
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
                cJSON* cJSON_num_entry = cJSON_GetObjectItemCaseSensitive(rxJSON, "length");

                if(cJSON_IsNumber(cJSON_num_entry)){
                    entry_len = (int)cJSON_num_entry->valuedouble;
                    printf("transaction log length %d\n", entry_len);

                    cJSON* cJSON_transaction_log_array = cJSON_GetObjectItemCaseSensitive(rxJSON, "transaction log");
                    printf("after get transaction log array\n");
                    if(cJSON_transaction_log_array != NULL){
                        int idx = 0;
                        int loop_num = (entry_len <= cJSON_GetArraySize(cJSON_transaction_log_array))?entry_len:cJSON_GetArraySize(cJSON_transaction_log_array);

                        for(; idx < loop_num; idx++){
                            printf("index:%d\n", idx);
                            cJSON* cJSON_log = cJSON_GetArrayItem(cJSON_transaction_log_array, idx);
                            if(cJSON_log != NULL){
                                cJSON* cJSON_log_content = cJSON_GetObjectItemCaseSensitive(cJSON_log, "log");
                                if(cJSON_log_content != NULL){
                                    printf("%s\n", cJSON_log_content->valuestring);
                                }else{
                                    printf("Error! no message\n");
                                }
                            }else{
                                printf("Error! Cannot get cJSON log\n");
                            }
                        }
                    }else{
                        printf("Error! Cannot get cJSON transaction log\n");
                    }
                }else{
                    printf("transaction length error\n");
                }
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_change_pw(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)MODIFYPW);

    char ssn[128] = {0};
    printf("Please input ssn: ");
    fgets(ssn, sizeof(ssn), stdin);

    //remove newline
    char* pos = strchr(ssn, '\n');
    if(pos != NULL) *pos = '\0';
    cJSON_AddItemToObject(root, "ssn", cJSON_CreateString(ssn));

    char new_pw[128] = {0};
    printf("Please input new password: ");
    fgets(new_pw, sizeof(new_pw), stdin);

    //remove newline
    pos = strchr(new_pw, '\n');
    if(pos != NULL) *pos = '\0';
    cJSON_AddItemToObject(root, "new password", cJSON_CreateString(new_pw));
#ifdef DEBUG
    print_preallocated(root);
#endif
    // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "ssn");
    cJSON_DetachItemFromObject(root, "new password");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_exit(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)EXIT);

#ifdef DEBUG
    print_preallocated(root);
#endif
    // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

/* Create a bunch of objects as demonstration. */
#ifdef DEBUG
static int print_preallocated(cJSON *root)
{
    /* declarations */
    char *out = NULL;
    char *buf = NULL;
    char *buf_fail = NULL;
    size_t len = 0;
    size_t len_fail = 0;

    /* formatted print */
    out = cJSON_Print(root);

    /* create buffer to succeed */
    /* the extra 5 bytes are because of inaccuracies when reserving memory */
    len = strlen(out) + 5;
    buf = (char*)malloc(len);
    if (buf == NULL)
    {
        printf("Failed to allocate memory.\n");
        exit(1);
    }

    /* create buffer to fail */
    len_fail = strlen(out);
    buf_fail = (char*)malloc(len_fail);
    if (buf_fail == NULL)
    {
        printf("Failed to allocate memory.\n");
        exit(1);
    }

        /* Print to buffer */
    if (!cJSON_PrintPreallocated(root, buf, (int)len, 1)) {
        printf("cJSON_PrintPreallocated failed!\n");
        if (strcmp(out, buf) != 0) {
            printf("cJSON_PrintPreallocated not the same as cJSON_Print!\n");
            printf("cJSON_Print result:\n%s\n", out);
            printf("cJSON_PrintPreallocated result:\n%s\n", buf);
        }
        free(out);
        free(buf_fail);
        free(buf);
        return -1;
    }

    /* success */
    printf("%s\n", buf);

    /* force it to fail */
    if (cJSON_PrintPreallocated(root, buf_fail, (int)len_fail, 1)) {
        printf("cJSON_PrintPreallocated failed to show error with insufficient memory!\n");
        printf("cJSON_Print result:\n%s\n", out);
        printf("cJSON_PrintPreallocated result:\n%s\n", buf_fail);
        free(out);
        free(buf_fail);
        free(buf);
        return -1;
    }

    free(out);
    free(buf_fail);
    free(buf);
    return 0;
}
#endif
