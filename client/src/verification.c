#include "verification.h"
#include "client_tx_rx_msg.h"

int verify_id(int sock, cJSON* send){
    int ret = -1;
    ret = tx_message_to_server(sock, send);
#if 1
   // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }
#endif

    return ret;
}

int verify_customer_selection(const char s){
   // return 0 if success
   return (s >= 'a' && s <= 'g') ? 0 : 1;
}

int verify_admin_selection(const char s){
   // return 0 if success
   return (s >= 'a' && s <= 'e') ? 0 : 1;
}

int verify_employee_selection(const char s){
   // return 0 if success
   return (s >= 'a' && s <= 'h') ? 0 : 1;
}

