#include <stdlib.h>
#include "employee_action.h"
#include "msg_typedef.h"

static void process_create_customer(int sock, cJSON* root);
static void process_delete_customer(int sock, cJSON* root);
static void process_create_account(int sock, cJSON* root);
static void process_delete_account(int sock, cJSON* root);
static void process_change_pw(int sock, cJSON* root);
static void process_transfer(int sock, cJSON* root);
static void process_view_balance(int sock, cJSON* root);
static void process_exit(int sock, cJSON* root);
#ifdef DEBUG
static int print_preallocated(cJSON *root);
#endif

void employee_action(int sock, char s, cJSON* root){

    switch(s){
        case 'a':
	        process_create_customer(sock, root);
            break;
        case 'b':
	        process_delete_customer(sock, root);
            break;
        case 'c':
	        process_create_account(sock, root);
            break;
        case 'd':
	        process_delete_account(sock, root);
            break;
        case 'e':
            process_change_pw(sock, root);
            break;
        case 'f':
            process_transfer(sock, root);
            break;
        case 'g':
            process_view_balance(sock, root);
            break;
        case 'h':
            process_exit(sock, root);
            break;
    }
}

static void process_create_customer(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)C_CUSTOMER);

    char account[128] = {0};
    printf("input name: ");
    fgets(account, sizeof(account), stdin);

    //remove newline
    char* pos = strchr(account, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "customer name", cJSON_CreateString(account));

    char pw[128] = {0};
    printf("input password: ");
    fgets(pw, sizeof(pw), stdin);

    //remove newline
    pos = strchr(pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "customer password", cJSON_CreateString(pw));

    char ssn[128] = {0};
    printf("input ssn: ");
    fgets(ssn, sizeof(ssn), stdin);

    //remove newline
    pos = strchr(ssn, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "ssn", cJSON_CreateString(ssn));

#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "customer name");
    cJSON_DetachItemFromObject(root, "customer password");
    cJSON_DetachItemFromObject(root, "ssn");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_delete_customer(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)D_CUSTOMER);

    char account[128] = {0};
    printf("input name: ");
    fgets(account, sizeof(account), stdin);

    //remove newline
    char* pos = strchr(account, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "customer name", cJSON_CreateString(account));
#if 0
    char pw[128] = {0};
    printf("input password: ");
    fgets(pw, sizeof(pw), stdin);

    //remove newline
    pos = strchr(pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "customer password", cJSON_CreateString(pw));

    char ssn[128] = {0};
    printf("input ssn: ");
    fgets(ssn, sizeof(ssn), stdin);

    //remove newline
    pos = strchr(ssn, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "ssn", cJSON_CreateString(ssn));
#endif
#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "customer name");
//    cJSON_DetachItemFromObject(root, "customer password");
//    cJSON_DetachItemFromObject(root, "ssn");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_create_account(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)C_CUSTOMER_ACCOUNT);

    char account[128] = {0};
    printf("input name: ");
    fgets(account, sizeof(account), stdin);

    //remove newline
    char* pos = strchr(account, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "customer name", cJSON_CreateString(account));

    char account_name[128] = {0};
    printf("input account name: ");
    fgets(account_name, sizeof(account_name), stdin);

    //remove newline
    pos = strchr(account_name, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "account name", cJSON_CreateString(account_name));

#if 0
    char pw[128] = {0};
    printf("input password: ");
    fgets(pw, sizeof(pw), stdin);

    //remove newline
    pos = strchr(pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "customer password", cJSON_CreateString(pw));

    char ssn[128] = {0};
    printf("input ssn: ");
    fgets(ssn, sizeof(ssn), stdin);

    //remove newline
    pos = strchr(ssn, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "ssn", cJSON_CreateString(ssn));
#endif
#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "customer name");
    cJSON_DetachItemFromObject(root, "account name");
//    cJSON_DetachItemFromObject(root, "customer password");
//    cJSON_DetachItemFromObject(root, "ssn");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_delete_account(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)D_CUSTOMER_ACCOUNT);

    char account[128] = {0};
    printf("input account: ");
    fgets(account, sizeof(account), stdin);

    //remove newline
    char* pos = strchr(account, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "account name", cJSON_CreateString(account));

#if 0
    char pw[128] = {0};
    printf("input password: ");
    fgets(pw, sizeof(pw), stdin);

    //remove newline
    pos = strchr(pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "customer password", cJSON_CreateString(pw));

    char ssn[128] = {0};
    printf("input ssn: ");
    fgets(ssn, sizeof(ssn), stdin);

    //remove newline
    pos = strchr(ssn, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "ssn", cJSON_CreateString(ssn));
#endif
#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "account name");
//    cJSON_DetachItemFromObject(root, "customer password");
//    cJSON_DetachItemFromObject(root, "ssn");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_change_pw(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)MODIFYPW);

    char name[128] = {0};
    printf("input name: ");
    fgets(name, sizeof(name), stdin);

    //remove newline
    char* pos = strchr(name, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "name", cJSON_CreateString(name));

    char ssn[128] = {0};
    printf("input ssn: ");
    fgets(ssn, sizeof(ssn), stdin);

    //remove newline
    pos = strchr(ssn, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "ssn", cJSON_CreateString(ssn));

    char old_pw[128] = {0};
    printf("input old password: ");
    fgets(old_pw, sizeof(old_pw), stdin);

    //remove newline
    pos = strchr(old_pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "old password", cJSON_CreateString(old_pw));

    char new_pw[128] = {0};
    printf("input new password: ");
    fgets(new_pw, sizeof(new_pw), stdin);

    //remove newline
    pos = strchr(new_pw, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "new password", cJSON_CreateString(new_pw));

#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "name");
    cJSON_DetachItemFromObject(root, "ssn");
    cJSON_DetachItemFromObject(root, "old password");
    cJSON_DetachItemFromObject(root, "new password");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_transfer(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)TRANSFER);

    char name[128] = {0};
    printf("input name: ");
    fgets(name, sizeof(name), stdin);

    //remove newline
    char* pos = strchr(name, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "name", cJSON_CreateString(name));

    char password[128] = {0};
    printf("input password: ");
    fgets(password, sizeof(password), stdin);

    //remove newline
    pos = strchr(password, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "password", cJSON_CreateString(password));

    char from[128] = {0};
    printf("input from account: ");
    fgets(from, sizeof(from), stdin);

    //remove newline
    pos = strchr(from, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "from account", cJSON_CreateString(from));

    char to[128] = {0};
    printf("input to account name: ");
    fgets(to, sizeof(to), stdin);

    //remove newline
    pos = strchr(to, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "to account", cJSON_CreateString(to));

    double money = 0.0;
    printf("How much ? ");
    scanf("%lf", &money);
    //eat up new line
    getchar();
    // !!!! validation for money
    cJSON_AddNumberToObject(root, "amount", money);

#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "name");
    cJSON_DetachItemFromObject(root, "password");
    cJSON_DetachItemFromObject(root, "from account");
    cJSON_DetachItemFromObject(root, "to account");
    cJSON_DetachItemFromObject(root, "amount");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_view_balance(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)BALANCE);

    char from[128] = {0};
    printf("input from account name: ");
    fgets(from, sizeof(from), stdin);

    //remove newline
    char* pos = strchr(from, '\n');
    if(pos != NULL) *pos = '\0';

    cJSON_AddItemToObject(root, "account", cJSON_CreateString(from));

#ifdef DEBUG
    print_preallocated(root);
#endif
    // send to server
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");
    cJSON_DetachItemFromObject(root, "account");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

static void process_exit(int sock, cJSON* root){

    cJSON_AddNumberToObject(root, "action", (int)EXIT);

#ifdef DEBUG
    print_preallocated(root);
#endif
    // !!!! validation for money
    int ret = tx_message_to_server(sock, root);
    (void)ret;

    cJSON_DetachItemFromObject(root, "action");

    // read from server
    cJSON* rxJSON = rx_message_from_server(sock);
    if(rxJSON != NULL){
        cJSON* result = cJSON_GetObjectItemCaseSensitive(rxJSON, "result");
        if(cJSON_IsNumber(result)){
            ret = (int)result->valuedouble;

            if(ret == 0){
                printf("Operation Success\n");
            }else{
                printf("Operation Failed\n");
            }
        }
    }else{
        perror("Fail to rx msg from server\n");
    }

    // reclaim resource
    cJSON_Delete(rxJSON);
}

/* Create a bunch of objects as demonstration. */
#ifdef DEBUG
static int print_preallocated(cJSON *root)
{
    /* declarations */
    char *out = NULL;
    char *buf = NULL;
    char *buf_fail = NULL;
    size_t len = 0;
    size_t len_fail = 0;

    /* formatted print */
    out = cJSON_Print(root);

    /* create buffer to succeed */
    /* the extra 5 bytes are because of inaccuracies when reserving memory */
    len = strlen(out) + 5;
    buf = (char*)malloc(len);
    if (buf == NULL)
    {
        printf("Failed to allocate memory.\n");
        exit(1);
    }

    /* create buffer to fail */
    len_fail = strlen(out);
    buf_fail = (char*)malloc(len_fail);
    if (buf_fail == NULL)
    {
        printf("Failed to allocate memory.\n");
        exit(1);
    }

        /* Print to buffer */
    if (!cJSON_PrintPreallocated(root, buf, (int)len, 1)) {
        printf("cJSON_PrintPreallocated failed!\n");
        if (strcmp(out, buf) != 0) {
            printf("cJSON_PrintPreallocated not the same as cJSON_Print!\n");
            printf("cJSON_Print result:\n%s\n", out);
            printf("cJSON_PrintPreallocated result:\n%s\n", buf);
        }
        free(out);
        free(buf_fail);
        free(buf);
        return -1;
    }

    /* success */
    printf("%s\n", buf);

    /* force it to fail */
    if (cJSON_PrintPreallocated(root, buf_fail, (int)len_fail, 1)) {
        printf("cJSON_PrintPreallocated failed to show error with insufficient memory!\n");
        printf("cJSON_Print result:\n%s\n", out);
        printf("cJSON_PrintPreallocated result:\n%s\n", buf_fail);
        free(out);
        free(buf_fail);
        free(buf);
        return -1;
    }

    free(out);
    free(buf_fail);
    free(buf);
    return 0;
}
#endif
