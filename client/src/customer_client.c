#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include "msg_typedef.h"
#include "manual.h"
#include "cJSON.h"
#include "verification.h"
#include "customer_action.h"

#define USERNAME_LEN 32
#define PASSWORD_LEN 32
#define MAX_FAIL 3

int main(int argc ,char* argv[]){

    if(argc != 3){
        printf("too few arguments, please input serer ip and port number\n");
        exit(-1);
    }

    struct hostent* server_ip = gethostbyname(argv[1]);
    if(NULL == server_ip){
        perror("gethostbyname return NULL\n");
        exit(-1);
    }
    int port_num = atoi(argv[2]);

    printf("server addr:port %s:%d\n", argv[1], port_num);
#if 0
    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    memcpy(&server_info.sin_addr, server_ip->h_addr, server_ip->h_length);

    // create client TCP socket
    int client_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(client_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }

    // connect to daytime server
    int ret = connect(client_soc, (struct sockaddr*)&server_info, sizeof(server_info));
    if(ret != 0){
        printf("Failed to connect to the server, host:%s\n", (char*)server_ip);
        close(client_soc);
        exit(-1);
    }

    char username[USERNAME_LEN] = {0};
    char password[PASSWORD_LEN] = {0};
    cJSON *root = cJSON_CreateObject();

    int try = 0;
 #endif

    int try = 0;
    int ret = 0;
    int client_soc = 0;
    cJSON *root = NULL;

    for(; try < MAX_FAIL; try++){
        // build IPv4 structure
        struct sockaddr_in server_info;
        memset(&server_info, 0, sizeof(server_info));

        server_info.sin_family = AF_INET;
        server_info.sin_port   = htons(port_num);
        memcpy(&server_info.sin_addr, server_ip->h_addr, server_ip->h_length);

        // create client TCP socket
        client_soc = socket(PF_INET, SOCK_STREAM, 0);
        if(client_soc == -1){
             perror("Failed to create client socket, return");
            exit(-1);
        }

        // connect to server
        ret = connect(client_soc, (struct sockaddr*)&server_info, sizeof(server_info));
        if(ret != 0){
            printf("Failed to connect to the server, host:%s\n", (char*)server_ip);
            close(client_soc);
            exit(-1);
        }

        char username[USERNAME_LEN] = {0};
        char password[PASSWORD_LEN] = {0};
        root = cJSON_CreateObject();

        print_input_username();
        fgets(username, sizeof(username), stdin);
        print_input_password();
        fgets(password, sizeof(password), stdin);
        //remove newline
        char* pos = strchr(username, '\n');
        if(pos != NULL) *pos = '\0';
        pos = strchr(password, '\n');
        if(pos != NULL) *pos = '\0';

        printf("verifying your id ...\n");

        cJSON_AddItemToObject(root, "customer name", cJSON_CreateString(username));
        cJSON_AddItemToObject(root, "customer password", cJSON_CreateString(password));
        cJSON_AddItemToObject(root, "who", cJSON_CreateString("CUSTOMER"));

        ret = verify_id(client_soc, root);

	printf("verify result %d\n", ret);

        if(ret == 0){
            break;
        }else{
	    cJSON_DetachItemFromObject(root, "customer name");
	    cJSON_DetachItemFromObject(root, "customer password");
	    cJSON_DetachItemFromObject(root, "who");
        }

        printf("failed to verify your id, try again \n");
    }

    if(try == MAX_FAIL && ret != 0){
        goto cleanup;
    }else{
        // while loop manual
        printf("verify ok\n");

        cJSON_AddNumberToObject(root, "client sock", client_soc);

        char selection;
        while(1){
            print_customer_manual();
            scanf("%c", &selection);
            getchar();//eat up new line
            if(verify_customer_selection(selection) == 0){
                customer_action(client_soc, selection, root);
                if(selection == 'g'){
                    goto cleanup;    //exit
                }

            }else{
                printf("input error, try again\n");
            }
        }
    }

cleanup:
    printf("Customer Bye ...\n");
    close(client_soc);
    cJSON_Delete(root);
    return 0;
}
