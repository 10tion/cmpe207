#include "manual.h"

void print_input_username(void){
    printf("Please input username: ");
}

void print_input_password(void){
    printf("Please input password: ");
}

void print_customer_manual(void){
    printf("Please select:\n");
    printf("a - withdraw\n");
    printf("b - deposit\n");
    printf("c - transfer\n");
    printf("d - view balance\n");
    printf("e - history\n");
    printf("f - change password\n");
    printf("g - logout\n");
}

void print_admin_manual(void){
    printf("Please select:\n");
    printf("a - create employee account\n");
    printf("b - delete employee account\n");
    printf("c - change employee password\n");
    printf("d - change admin password\n");
    printf("e - logout\n");
}

void print_employee_manual(void){
    printf("Please select:\n");
    printf("a - create customer\n");
    printf("b - delete customer\n");
    printf("c - create account\n");
    printf("d - delete account\n");
    printf("e - change password\n");
    printf("f - transfer\n");
    //printf("g - view balance\n");
    printf("h - logout\n");
}
