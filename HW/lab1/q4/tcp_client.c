#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>

void usage_manual(void);

int main(int argc, char* argv[]){
    
    // pass tcp server ip, tcp server port, and integer
    if(argc != 4){
        usage_manual();
        exit(-1);
    }
    
    /* ******************************************
     * man gethostbyname():
     * if input is server name, return server ip;
     * if input is ip, no loopup, return ip
     * ******************************************
     */
    struct hostent* server_ip = gethostbyname(argv[1]);
    if(NULL == server_ip){
        perror("gethostbyname return NULL\n");
        exit(-1);
    }
    
    int port_num = atoi(argv[2]);
    int input    = atoi(argv[3]);

    printf("connecting to server:%d, and send %d, ",port_num, input);

    // build IPv4 structure
    struct sockaddr_in server_info;
    // reset the server_info
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    memcpy(&server_info.sin_addr, server_ip->h_addr, server_ip->h_length);
   
    // create client TCP socket
    int client_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(client_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }
    
    // connect to daytime server
    int ret = connect(client_soc, (struct sockaddr*)&server_info, sizeof(server_info));
    if(ret != 0){
        perror("Failed to connect to the daytime server\n");
        close(client_soc);
        exit(-1);
    }

    int num_bytes = sendto(client_soc, 
                           &input, 
                           sizeof(input), 
                           0, 
                           (struct sockaddr*)&server_info, 
                           (socklen_t)sizeof(struct sockaddr));

    if(num_bytes == -1){
        perror("Failed to send message to server\n");
        close(client_soc);
        exit(-1);
    }
    
    int rcv_num = 0;
    socklen_t rcv_size = 0;
    num_bytes = recvfrom(client_soc,
                         (char*)&rcv_num,
                         sizeof(rcv_num),
                         0,
                         (struct sockaddr*)&server_info,
                         &rcv_size);
    if(num_bytes == -1){
        perror("Failed to receive message from server\n");
        close(client_soc);
        exit(-1);
    }
    
    printf("receive %d from server.\n", rcv_num);
    close(client_soc);

    return 0;    
}

/*
 * Out put manual
 * */
void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server ip address -\n");
    printf("- server port number -\n");
    printf("- an integer -\n");
}
