#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

/* *
 * TCP server:
 * socket -> bind -> listen -> accept -> read -> write
 *
 * */
#define QUEUE_LEN 32

void usage_manual(void);

int main(int argc, char* argv[]){

    // pass tcp listening port number 
    if(argc != 2){
        usage_manual();
        exit(-1);
    }   
    
    int port_num = atoi(argv[1]);
    printf("listening on port %d\n\n", port_num);

    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    server_info.sin_addr.s_addr = INADDR_ANY;
   
    // create server TCP socket
    int server_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(server_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }

    // bind the socket 
    int ret = bind(server_soc, (struct sockaddr *)&server_info, sizeof(server_info)); 
    if(ret != 0){
        printf("cannot bind to server:%d\n", port_num);
        close(server_soc);
        exit(-1);
    }

    while(1){
        printf("waiting for input from client, ");
        // listen socket 
        ret = listen(server_soc, QUEUE_LEN);
        if(ret != 0){
            printf("cannot listen on %d\n", port_num);
            close(server_soc);
            exit(-1);
        }

        printf("listen socket number is:%d\n", server_soc);
        // accept connection
        struct sockaddr_in client_info;
        memset(&client_info, 0, sizeof(client_info));
        socklen_t client_addr_len = sizeof(client_info);
        
        int accept_soc = accept(server_soc, (struct sockaddr*)&client_info, &client_addr_len);
        if(accept_soc < 0){
            printf("accept failed, %s\n", strerror(errno));    
            close(server_soc);
            exit(-1);
        }
        printf("accept socket number is:%d\n", accept_soc);

        // read from client
        int rcv = 0;
        int num_bytes = read(accept_soc, &rcv, sizeof(rcv));
        if(num_bytes < 0){
            printf("read failed\n");
            close(server_soc);
            close(accept_soc);
            exit(-1);
        }

        printf("receive %d from client\n\n", rcv);
        rcv--;
        // write to client
        num_bytes = write(accept_soc, &rcv, sizeof(rcv));
        if(num_bytes < 0){
            printf("write failed\n");
            close(server_soc);
            close(accept_soc);
            exit(-1);
        }

        close(accept_soc);
    }
    return 0;
}

/*
 * Out put manual
 * */
void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server port number -\n");
}
