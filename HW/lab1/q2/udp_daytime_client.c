#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>

#define DAYTIME_SER_PORT    13 

int main(int argc, char* argv[]){
    
    if(argc != 2){
        perror("please input daytime serer ip or name\n");
        exit(-1);
    }
    
    /* ******************************************
     * man gethostbyname():
     * if input is server name, return server ip;
     * if input is ip, no loopup, return ip
     * ******************************************
     */
    struct hostent* server_ip = gethostbyname(argv[1]);
    if(NULL == server_ip){
        perror("gethostbyname return NULL\n");
        exit(-1);
    }
    
    // build IPv4 structure
    struct sockaddr_in server_info;
    // reset the server_info
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(DAYTIME_SER_PORT);
    memcpy(&server_info.sin_addr, server_ip->h_addr, server_ip->h_length);
   
    // create client UDP socket
    int client_soc = socket(PF_INET/*AF_INET is ok*/, SOCK_DGRAM, 0);
    if(client_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }
    
    // connect to daytime server
    int ret = connect(client_soc, (struct sockaddr*)&server_info, sizeof(server_info));
    if(ret != 0){
        perror("Failed to connect to the daytime server\n");
        close(client_soc);
        exit(-1);
    }
     
    const char* msg = "I send you any messages, and you provide me the daytime, Haha\n";
    int num_bytes = sendto(client_soc, 
                           msg, 
                           strlen(msg), 
                           0, 
                           (struct sockaddr*)&server_info, 
                           (socklen_t)sizeof(struct sockaddr));

    if(num_bytes == -1){
        perror("Failed to send message to daytime server\n");
        close(client_soc);
        exit(-1);
    }
    
    char daytime_buf[1024] = {0};
    socklen_t recv_size = 0;
    num_bytes = recvfrom(client_soc,
                         daytime_buf,
                         sizeof(daytime_buf),
                         0,
                         (struct sockaddr*)&server_info,
                         &recv_size);
    if(num_bytes == -1){
        perror("Failed to send message to daytime server\n");
        close(client_soc);
        exit(-1);
    }

    printf("return value from the server:%s\n", daytime_buf);
    close(client_soc);
    return 0;    
}
