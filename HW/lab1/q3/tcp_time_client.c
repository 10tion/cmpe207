#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>

#define TIME_SER_PORT   37
#define UNIX_OFFSET     2208988800U

int main(int argc, char* argv[]){
    
    if(argc != 3){
        printf("too few arguments, please input daytime serer ip or name\n");
        exit(-1);
    }
    
    /* ******************************************
     * man gethostbyname():
     * if input is server name, return server ip;
     * if input is ip, no loopup, return ip
     * ******************************************
     */
    int idx = 1; 
    unsigned int final_time[2] = {0};

    for(; idx < argc; idx++){
        printf("argv:%s\n", argv[idx]);
        struct hostent* server_ip = gethostbyname(argv[idx]);
        if(NULL == server_ip){
            perror("gethostbyname return NULL\n");
            exit(-1);
        }
        
        printf("request time from server ip:%s\n", server_ip->h_name);
        // build IPv4 structure
        struct sockaddr_in server_info;
        memset(&server_info, 0, sizeof(server_info));

        server_info.sin_family = AF_INET;
        server_info.sin_port   = htons(TIME_SER_PORT);
        memcpy(&server_info.sin_addr, server_ip->h_addr, server_ip->h_length);
   
        // create client UDP socket
        int client_soc = socket(PF_INET, SOCK_STREAM, 0);
        if(client_soc == -1){
            perror("Failed to create client socket, return");
            exit(-1);
        }
        
        // connect to daytime server
        int ret = connect(client_soc, (struct sockaddr*)&server_info, sizeof(server_info));
        if(ret != 0){
            printf("Failed to connect to the daytime server, host:%s\n", (char*)server_ip);
            close(client_soc);
            exit(-1);
        }

        unsigned int time;
        int num_bytes = write(client_soc, (char*)&time, sizeof(time));
        if(num_bytes == -1){
            printf("Failed to send message to time server:%s\n", (char*)(server_ip->h_addr));
            close(client_soc);
            exit(-1);
        }

        num_bytes = read(client_soc, (char*)&time, sizeof(time));
        if(num_bytes == -1){
            printf("Failed to read message from time server:%s\n", (char*)(server_ip->h_addr));
            close(client_soc);
            exit(-1);
        }       
        unsigned int net_time = ntohl(time) - UNIX_OFFSET;
         
        printf("Time:%x, %s\n", ntohl(time), ctime((time_t*)&net_time));
        final_time[idx] = net_time; 

        if(idx != 2)
            sleep(5);

        close(client_soc);
    }
    
    printf("The difference between these two servers is:%u sec\n", final_time[2] - final_time[1]);
    
    return 0;    
}
