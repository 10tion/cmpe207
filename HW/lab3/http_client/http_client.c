#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>

#define MAX_REQ_LEN     512
#define MAX_RCV_LEN     512
#define HTML_HEADER_LEN 1024
#define FILE_PATH         "./" 
#define DEFAULT_FILE_NAME "index.html"

static void usage_manual(void);
static void http_get(int sock, char* url);
#if 0
static int parse_http_response(char* http_response);
#endif

int main(int argc, char* argv[]){
    
    // pass tcp server ip, tcp server port, and URL 
    if(argc != 4){
        usage_manual();
        exit(-1);
    }

    /* ******************************************
     * man gethostbyname():
     * if input is server name, return server ip;
     * if input is ip, no loopup, return ip
     * ******************************************
     */
    struct hostent* server_ip = gethostbyname(argv[1]);
    if(NULL == server_ip){
        perror("gethostbyname return NULL\n");
	printf("ERROR: %s\n", strerror(errno));
        exit(-1);
    }
    
    int port_num = atoi(argv[2]);

    // build IPv4 structure
    struct sockaddr_in server_info;
    // reset the server_info
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    memcpy(&server_info.sin_addr, server_ip->h_addr, server_ip->h_length);
   
    // create client TCP socket
    int client_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(client_soc < 0){
	printf("ERROR: %s\n", strerror(errno));
        exit(-1);
    }
    
    // connect to http server
    int ret = connect(client_soc, (struct sockaddr*)&server_info, sizeof(server_info));
    if(ret < 0){
	printf("ERROR: %s\n", strerror(errno));
        close(client_soc);
        exit(-1);
    }

    int html_file_fd;
    time_t start_time = time(NULL);

    // SEND HTTP [GET]
    http_get(client_soc, argv[3]);

    char buf[MAX_RCV_LEN] = {0};
    char html_header[HTML_HEADER_LEN] = {0};     
    int is_first_pkg = 1;
    int count = 0;
    int size = 0;

    while ((count = recv(client_soc, buf, MAX_RCV_LEN, 0)) > 0) {
        size += count;
        
        if(is_first_pkg){   // ignore html header
            is_first_pkg = 0;

    
            char file_full_path[128] = {0};

	    if((argv[3] != NULL) && (strlen(argv[3]) == 1) && (argv[3][0] == '/')){
	    	sprintf(file_full_path, "%s%s", FILE_PATH, DEFAULT_FILE_NAME);
	    }else{
	    	sprintf(file_full_path, "%s%s", FILE_PATH, argv[3]);
	    }

	    html_file_fd = open(file_full_path, O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);
	    if(html_file_fd < 0){
		printf("Error :%s\n", strerror(errno));    
		goto cleanup;
	    }

            char* html_start = strchr(buf, '<');
            
            if(html_start == NULL){
                printf("buf :%s\n", buf);
            }else{
                memcpy(html_header, buf, html_start - buf);            
                html_header[html_start - buf] = '\0';
                printf("--- HTML Header: ---\n");
                printf("%s", html_header);

                write(html_file_fd, html_start, count - (html_start - buf));   
            }
        }else{
            write(html_file_fd, buf, count);
        }

        memset(buf, 0, MAX_RCV_LEN);
        memset(html_header, 0, HTML_HEADER_LEN);
    }
    sleep(1);
    printf("time last:%d second\n", (int)(time(NULL) - start_time));
    printf("recv file size: %d Bytes\n", size);
cleanup:    
    close(html_file_fd);
    close(client_soc);

    return 0;    
}

static void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server ip address -\n");
    printf("- server port number -\n");
    printf("- URL -\n");
}

static void http_get(int sock, char* url){

    char request[MAX_REQ_LEN] = {0};

    sprintf(request, "GET %s HTTP/1.0\r\n\r\n", url);
    send(sock, request, sizeof(request), 0);
}

#if 0
static int parse_http_response(char* http_response){
    int ret = -1;

    if(http_response != NULL){
        char* http_protocol_ver = strtok(http_response, " ");
        (void) http_protocol_ver;	//unused for now
        char* http_status = strtok(NULL, " ");
	if(strncasecmp(http_status, "200", strlen("200")) == 0){
            ret = atoi(http_status);
        }
    }
    
    return ret;
}
#endif
