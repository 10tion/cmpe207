#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

typedef struct http_header{
    int status_code;
    const char* status_str;

    /* *
     * ADD MORE FIELDS HERE
     * time_t date:
     * const char* server:
     * ...
     * */
} http_header_t;

typedef struct http_err_body{
    int status_code;
    const char* display_str;

    /* *
     * ADD MORE FIELDS HERE
     * time_t date:
     * const char* server:
     * ...
     * */
} http_err_body_t;

/* *
 * TCP server:
 * socket -> bind -> listen -> accept -> read -> write
 *
 * */
#define QUEUE_LEN 32
#define MAX_FILE_NAME_LEN   32
#define MAX_FILE_SIZE       (1024*20)
#define MAX_TRANSMIT_SIZE   512
#define MAX_NUM_OF_THREAD   16

/* *
 * HTTP protocol
 * */
#define HTTP_CLIENT_ERR_BAD_REQUEST      400
#define HTTP_CLIENT_ERR_FORBIDDEN        403
#define HTTP_CLIENT_ERR_FILE_NOT_FOUND   404
#define HTTP_SUCCESS_200                 200
#define HTTP_HEADER_LENGTH               64 
#define HTTP_REQUEST_BUF_SIZE            512 
#define HTTP_SERVER_FILE_PREFIX          "./" //by default, file is stored the same directory as server code
#define HTTP_DEFAULT_FILE_NAME           "index.html"
#define SIZE_OF_G_HTTP_HEADER_ARRAY \
		( sizeof(g_http_header_array)/sizeof(http_header_t) )
#define SIZE_OF_G_HTTP_ERR_BODY_ARRAY \
		( sizeof(g_http_err_body_array)/sizeof(http_err_body_t) )

http_header_t g_http_header_array[] = {
    { HTTP_SUCCESS_200,               "HTTP/1.0 200 OK\r\n\r\n"},
    { HTTP_CLIENT_ERR_BAD_REQUEST,    "HTTP/1.0 400 Bad Request\r\n\r\n"},
    { HTTP_CLIENT_ERR_FORBIDDEN,      "HTTP/1.0 403 Forbidden\r\n\r\n"},
    { HTTP_CLIENT_ERR_FILE_NOT_FOUND, "HTTP/1.0 404 Not Found\r\n\r\n"}
};

char g_bad_req_msg[] = "<!DOCTYPE HTML PUBLIC '-//IETF//DTD HTML 2.0//EN'> \r\n \
<html><head> \r\n \
<title>400 Bad Request</title> \r\n \
</head><body> \r\n \
<h1>400 Bad Request</h1> \r\n \
<p>Your browser sent a request that this server could not understand.<br /> \r\n \
</p> \r\n \
<hr> \r\n \
<address>CMPE-207 Server at 127.0.1.1 Port 80</address> \r\n \
</body></html> \r\n"; 

char g_forbidden_msg[] = "<!DOCTYPE HTML PUBLIC '-//IETF//DTD HTML 2.0//EN'> \r\n \
<html><head> \r\n \
<title>403 Forbidden</title> \r\n \
</head><body> \r\n \
<h1>403 Forbidden</h1> \r\n \
<p>You don't have permission to access /index.html on this server.<br /> \r\n \
</p> \r\n \
<hr> \r\n \
<address>CMPE-207 Server at localhost Port 80</address> \r\n \
</body></html> \r\n"; 

char g_not_found_msg[] =  "<!DOCTYPE HTML PUBLIC '-//IETF//DTD HTML 2.0//EN'> \r\n \
<html><head> \r\n \
<title>404 NOT FOUND</title> \r\n \
</head><body> \r\n \
<h1>404 NOT FOUND</h1> \r\n \
<p> \r\n \
Your browser sent a request that this server could not find on this server.<br /> \r\n \
</p> \r\n \
<hr> \r\n \
<address>CMPE-207 Server at localhost Port 80</address> \r\n \
</body></html> \r\n"; 

http_err_body_t g_http_err_body_array[] = {
    { HTTP_CLIENT_ERR_BAD_REQUEST,    g_bad_req_msg},
    { HTTP_CLIENT_ERR_FORBIDDEN,      g_forbidden_msg},
    { HTTP_CLIENT_ERR_FILE_NOT_FOUND, g_not_found_msg}
};

/* *
 * FUNCTION prototype 
 * */
static void usage_manual(void);
static void send_http_header(int sock, int status_code);
static int search_http_header(int status_code);
static int search_http_body(int status_code);
static int parse_http_req(int accept_soc, char* file_name, int file_name_len);
static void send_http_body(int accept_soc, int status_code, const char* file_name);
static void* http_process(void* soc);

int main(int argc, char* argv[]){

    // pass tcp listening port number 
    if(argc != 2){
        usage_manual();
        exit(-1);
    }   
    
    int port_num = atoi(argv[1]);
    printf("listening on port %d\n\n", port_num);

    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    server_info.sin_addr.s_addr = INADDR_ANY;
   
    // create server TCP socket
    int server_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(server_soc < 0){
        printf("ERROR: %s\n", strerror(errno));
        exit(-1);
    }

    // bind the socket 
    int ret = bind(server_soc, (struct sockaddr *)&server_info, sizeof(server_info)); 
    if(ret < 0){
        printf("ERROR: %s\n", strerror(errno));
        close(server_soc);
        exit(-1);
    }
     
    pthread_t sniffer_thread[MAX_NUM_OF_THREAD] = {0};
    int thread_idx = 0;

    while(1){
        printf("waiting for incomming http client, ");

        // listen socket 
        ret = listen(server_soc, QUEUE_LEN);
        if(ret < 0){
            printf("ERROR: %s\n", strerror(errno));
            close(server_soc);
            exit(-1);
        }

        printf("listen socket number is:%d\n", server_soc);

        // accept connection
        struct sockaddr_in client_info;
        memset(&client_info, 0, sizeof(client_info));
        socklen_t client_addr_len = sizeof(client_info);
        
        int accept_soc = accept(server_soc, (struct sockaddr*)&client_info, &client_addr_len);
        if(accept_soc < 0){
            printf("ERROR: %s\n", strerror(errno));
            close(server_soc);
            exit(-1);
        }
        printf("accept sock is:%d\n", accept_soc);

        // create thread to process incoming connection
        if(pthread_create(&sniffer_thread[thread_idx++], NULL, http_process, (void*)&accept_soc) != 0){
            printf("ERROR: %s\n", strerror(errno));
            exit(-1);
        }
    }

    return 0;
}

static void* http_process(void* soc){

    char req_file_name[64] = {0};

    int http_status_code = parse_http_req(*((int*)soc), req_file_name, sizeof(req_file_name));
    printf("http_status_code:%d\n", http_status_code);

    send_http_header(*((int*)soc), http_status_code);
    send_http_body(*((int*)soc), http_status_code, req_file_name);

    return NULL;
}

static void usage_manual(void){
    printf("\n");
    printf("PLEASE PROVIDE LISTENING PORT NUMBER\n");
    printf("\n");
}

static int search_http_header(int status_code){

    int idx = -1;
    int loop = 0;
 
    for(; loop < SIZE_OF_G_HTTP_HEADER_ARRAY; loop++){
        if(status_code == g_http_header_array[loop].status_code){
            idx = loop;
            break;
        }
    }
    
    return idx; 
}

static int search_http_body(int status_code){

    int idx = -1;
    int loop = 0;
 
    for(; loop < SIZE_OF_G_HTTP_ERR_BODY_ARRAY; loop++){
        if(status_code == g_http_err_body_array[loop].status_code){
            idx = loop;
            break;
        }
    }
    
    return idx; 
}

static void send_http_header(int sock, int status_code){

    char http_header[HTTP_HEADER_LENGTH] = {0};
    int http_header_idx = search_http_header(status_code);
    
    if(http_header_idx != -1){
        sprintf(http_header, "%s", g_http_header_array[http_header_idx].status_str);
        send(sock, http_header, strlen(http_header), 0);
    }
}

static void send_http_body(int accept_soc, int status_code, const char* file_name){

    char file_content[MAX_FILE_SIZE] = {0};
    
    if(status_code == HTTP_SUCCESS_200){
        // prepare to write 
        int fd = open(file_name, O_RDONLY);
        if(fd < 0){
            printf("ERROR: %s\n", strerror(errno)); 
            close(accept_soc);
            exit(-1);
        }
        //printf("fd is %d\n", fd);

        // read file content 
        int num_bytes = read(fd, file_content, MAX_FILE_SIZE);
        if(num_bytes < 0){
            printf("ERROR: %s\n", strerror(errno));
            close(accept_soc);
            close(fd);
            exit(-1);
        }

        char* start = file_content;
        char* end = start + num_bytes - 1;
        
        // send file content to client
        int len = 0; 
        while(start < end){ 
            if((end - start + 1) > MAX_TRANSMIT_SIZE){
                len = MAX_TRANSMIT_SIZE;    
            }else{
                len = end - start + 1;    
            }
            
            // write back to client
            printf("write %d to client\n", len);

            int send_num = write(accept_soc, start, len); 
            if(send_num != len){
                printf("write error\n");
                close(accept_soc);
                close(fd);
                exit(-1);
            }
            
            start += len;
        }

        printf("completed send\n");
        close(fd);
    }else{
	// send error msg
	int msg_idx = search_http_body(status_code);
        if(msg_idx != -1){
	    write(accept_soc, g_http_err_body_array[msg_idx].display_str, strlen(g_http_err_body_array[msg_idx].display_str));
        }else{
            perror("No match msg error idx\n");
        } 
    }
    
    // remember to close accept socket
    printf("close socket\n");
    close(accept_soc);
}

/*
 * ONLY PARSE 1st & 2nd elements
 */
static int parse_http_req(int accept_soc, char* file_name, int file_name_len){
     
    char http_request[HTTP_REQUEST_BUF_SIZE] = {0};
    char* method = NULL;
    char* f_name = NULL;
    int status_code = HTTP_SUCCESS_200; 
    
    if(file_name == NULL){
        perror("NULL POINTER EXCEPTION");
        close(accept_soc);
        exit(-1);
    }
    
    memset(file_name, 0, file_name_len);
 
    // get http request from client
    int num_bytes = read(accept_soc, http_request, sizeof(http_request));
    if(num_bytes < 0){
        printf("ERROR: %s\n", strerror(errno));
        close(accept_soc);
        exit(-1);
    }
    
    method = strtok(http_request, " ");
    if(method != NULL){
        if(strncasecmp(method, "GET", strlen("GET")) != 0){
            status_code = HTTP_CLIENT_ERR_BAD_REQUEST; 
        }else{
	    // HTTP [GET]
	    f_name = strtok(NULL, " ");
            
            if((f_name != NULL) && (f_name[0] == '/') && (strlen(f_name) == 1)){    //if client request '/'
               snprintf(file_name, file_name_len, "%s%s", HTTP_SERVER_FILE_PREFIX, HTTP_DEFAULT_FILE_NAME);
            }else if ((f_name != NULL) && (f_name[0] == '/') && (strlen(f_name) > 1)){    //if client request '/filename'
               snprintf(file_name, file_name_len, "%s%s", HTTP_SERVER_FILE_PREFIX, (f_name+1)); 
            }else if((f_name != NULL) && (f_name[0] != '/')){    //if client request 'filename'
               snprintf(file_name, file_name_len, "%s%s", HTTP_SERVER_FILE_PREFIX, f_name);
            } 

            if(file_name[0] != '\0'){
	        if(access(file_name, F_OK) != 0)    // file not found
                    status_code = HTTP_CLIENT_ERR_FILE_NOT_FOUND;
                else if(access(file_name, R_OK) != 0)    // file cannot read
                    status_code = HTTP_CLIENT_ERR_FORBIDDEN;
            }
        }
    }else{
	status_code = HTTP_CLIENT_ERR_BAD_REQUEST; 
    }    

    return status_code;
}
