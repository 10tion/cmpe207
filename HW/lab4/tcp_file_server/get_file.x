/*
 * Specification of remote date and time service 
 * bindate() which returns the binary time and date (no args).
 * This file is the input to rpcgen 
 */

program GETFILEPROG { /* remote program name (not used)*/
        version GETFILEVERS { /* declaration of program version number*/
        string GET_FILE_CONTENT(string filename) = 1; /* procedure number = 1 */
    } = 3; /* definition of program version = 1*/
} = 4;/* remote program number (must be unique)*/
