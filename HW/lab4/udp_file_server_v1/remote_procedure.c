#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <error.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

static off_t file_size(const char* filename);

char* g_file_content = NULL;

char** get_file_content_3_svc(const char* filename){

    assert(filename);

    if(g_file_content != NULL){
        free(g_file_content);
        g_file_content = NULL;
    }

    // open file
    int fd = open((filename), O_RDONLY);
    if(fd < 0){
        printf("error to open file:%s, %s\n", filename, strerror(errno));
        exit(-1);
    }

    off_t file_len = file_size(filename);
    printf("file length: %d\n", (int)file_len);
    // malloc space
    g_file_content = (char*)malloc(file_len);
    memset(g_file_content, 0, sizeof(file_len));

    // read file
    int num_bytes = read(fd, g_file_content, file_len);
    if(num_bytes < 0){
        printf("read failed %s\n", strerror(errno));
        free(g_file_content);
        exit(-1);
    }

    printf("%d, %d\n", num_bytes, (int)file_len);

    g_file_content[num_bytes] = '\0';
    // return a pointer to the space
    return &g_file_content;
}

static off_t file_size(const char* filename){
    struct stat st;

    if(stat(filename, &st) == 0)
        return st.st_size;

    return -1;
}
