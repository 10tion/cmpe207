/****************************************************/
/* rdate.c - client program for remote date service */
/****************************************************/

#include <stdio.h>
#include <rpc/rpc.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "get_file.h"

static void write_to_file(const char* filename, char* buf);

int main(int argc, char *argv[]) {
    CLIENT *cl = NULL;
    char *server = NULL;
    char *file_name = NULL;
    char **file_content = NULL;

    if (argc != 3) {
        fprintf(stderr, "usage: %s hostname\n", argv[0]);
        exit(1);
    }

    server = argv[1];
    file_name = argv[2];

    /* create client handle */
    if ((cl = clnt_create(server, GETFILEPROG, GETFILEVERS, "udp")) == NULL) {
        /* couldn't establish connection with server */
        printf("can't establish connection with host %s\n", server);
        exit(2);
    }

    /* first call the remote procedure */
    file_content = get_file_content_3(file_name, cl);
    if (file_content == NULL){
        printf(" remote procedure get_file_content_1 failure\n");
        exit(3);
    }

    //printf("file_content: %s", *file_content);
    //printf("size %d\n", (int)strlen(*file_content));

    /* save file*/
    write_to_file(file_name, *file_content);

    clnt_destroy(cl); /* done with handle */
    return 0;
}

static void write_to_file(const char* filename, char* buf){

    if(filename != NULL){
        // prepare to write
        int fd = open(filename, O_CREAT|O_WRONLY, S_IRUSR|S_IRGRP);
        if(fd < 0){
            printf("error to open file: %s\n", filename);
            exit(-1);
        }

        write(fd, buf, (int)strlen(buf));
    }
}
