/****************************************************/
/* rdate.c - client program for remote date service */
/****************************************************/

#include <stdio.h>
#include <rpc/rpc.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "get_file.h"

/*
// package for sliced file
struct pkg {
    short seq;
    short maxSeq;
    char filename[32];
    char buf[512];
};

// request struct from client
struct req {
    short seq;
    char filename[32];
};
*/

static void write_to_file(const char* filename, char* buf);

int main(int argc, char *argv[]) {
    CLIENT *cl = NULL;
    char *server = NULL;
    char *file_name = NULL;
    struct pkg *package;

    if (argc != 3) {
        fprintf(stderr, "usage: %s hostname\n", argv[0]);
        exit(1);
    }

    server = argv[1];
    file_name = argv[2];

    /* create client handle */
    if ((cl = clnt_create(server, GETFILEPROG, GETFILEVERS, "udp")) == NULL) {
        /* couldn't establish connection with server */
        printf("can't establish connection with host %s\n", server);
        exit(2);
    }

    /* get the sliced sequence number of the file */
    //int local_seq = 0;
    int local_maxSeq = 0;

    // initialize request
    struct req request = {0, ""};
    memcpy(request.filename, file_name, strlen(file_name));

    /* send request and parse the package received */
    do {
        /* cannot get package from remote */
        if ((package = get_file_content_3(&request, cl)) == NULL) {
            printf(" Error occured in remote procedure 1\n");
            break;
        }

        /* remote handle error occured */
        if ((package->seq < 0) || (strcmp(package->filename, file_name))) {
            printf("seq: %d \n", package->seq);
            printf("%s %s", package->filename, file_name);
            printf(" Error occured in remote procedure 2\n");
            break;
        }
        printf("seq number: %d \n", package->seq);
        printf("maxseq number %d \n", package->maxSeq);
        local_maxSeq = package->maxSeq;

        // sequence number matched, write to file
        if(package->seq == request.seq) {
            write_to_file(file_name, package->buf);
            request.seq += 1;
        }

    } while(request.seq < local_maxSeq);

    clnt_destroy(cl); /* done with handle */
    return 0;
}

static void write_to_file(const char* filename, char* buf){
if(filename != NULL){
        // prepare to write
        int fd = open(filename, O_CREAT|O_WRONLY|O_APPEND, 0666);
        if(fd < 0){
            printf("error to open file: %s\n", filename);
            exit(-1);
        }
        printf("buf: %s\n", buf);
        write(fd, buf, strlen(buf));
        close(fd);
    }
}
