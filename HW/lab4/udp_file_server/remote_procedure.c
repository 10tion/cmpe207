#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <error.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <math.h>


// package for sliced file
struct pkg {
    short seq;
    short maxSeq;
    char filename[32];
    char buf[512];
    
};

// request struct from client
struct req {
    short seq;
    char filename[32];
};


static off_t file_size(const char* filename);

char* g_file_content = NULL;

struct pkg *get_file_content_3_svc(struct req *request) {

    if(g_file_content != NULL){
        free(g_file_content);
        g_file_content = NULL;
    }

    // open file
    int fd = open((request->filename), O_RDONLY);
    if(fd < 0){
        printf("error to open file:%s, %s\n", request->filename, strerror(errno));
        exit(-1);
    }

    off_t file_len = file_size(request->filename);
    printf("file length: %d\n", (int)file_len);
    // malloc space
    g_file_content = (char*)malloc(file_len);
    memset(g_file_content, 0, sizeof(file_len));

    // read file
    int num_bytes = read(fd, g_file_content, file_len);
    if(num_bytes < 0){
        printf("read failed %s\n", strerror(errno));
        free(g_file_content);
        exit(-1);
    }
    
    //printf("%s \n", g_file_content);

    printf("%d, %d\n", num_bytes, (int)file_len);

    g_file_content[num_bytes] = '\0';

    // sliced number of file
    int maxSeq = ceil(((double) file_len) / 512);

    struct pkg *package = (struct pkg *) malloc (sizeof(struct pkg));
    
    package->seq = -1;
    package->maxSeq = maxSeq;
    memset(package->filename, 0, 32);
    memset(package->buf, 0, 512);
    

    // request contain wrong sequence number
    if(request->seq > maxSeq) return package;

    package->seq = request->seq;
    package->maxSeq = maxSeq;
    memcpy(package->filename, request->filename, 32);
    
    char *ptr = g_file_content;
    ptr += (request->seq) * 512;
    
    strncpy(package->buf, ptr, 512);
    printf("%s \n", package->buf);
    // return max sequence number of package
    return package;
}


static off_t file_size(const char* filename){
    struct stat st;

    if(stat(filename, &st) == 0)
        return st.st_size;

    return -1;
}
