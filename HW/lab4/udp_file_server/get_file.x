/*
 * Specification of remote date and time service 
 * bindate() which returns the binary time and date (no args).
 * This file is the input to rpcgen 
 */

struct pkg {
    short seq;
    short maxSeq;
    char filename[32];
    char buf[512];
};

struct req {
    short seq;
    char filename[32];
};

program GETFILEPROG { /* remote program name (not used)*/
        version GETFILEVERS { /* declaration of program version number*/
        pkg GET_FILE_CONTENT(req) = 1; /* procedure number = 1 */
    } = 3; /* definition of program version = 1*/
} = 4;/* remote program number (must be unique)*/
