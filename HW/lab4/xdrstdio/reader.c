#include <stdio.h>

int main()       /* reader.c */
{
    int i, j;

    for (j = 0; j < 8; j++) {
        if (fread((char *) &i, sizeof(i), 1, stdin) != 1) {
            fprintf(stderr, "failed!\n");
            return(1);
        }
    printf("%d ", i);
    }
    printf("\n");
    return (0);
}
