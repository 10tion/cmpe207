#include <stdio.h>

int main()             /* writer.c */
{
    int i;

    for (i = 0; i < 8; i++) {
        char c = 'a';
        fwrite(&c, sizeof(c), 1, stdout);
        if (fwrite((char *) &i, sizeof(i), 1, stdout) != 1) {
            fprintf(stderr, "failed!\n");
            return(1);
        }

    }
    return(0);
}
