#include <stdio.h>
#include <rpc/rpc.h>    /* xdr is a sub-library of rpc */

int main()                /* reader.c */
{
    XDR xdrs;
    int i, j;

    xdrstdio_create(&xdrs, stdin, XDR_DECODE);

    for (j = 0; j < 8; j++) {
        if (!xdr_int(&xdrs, &i)) {
            fprintf(stderr, "failed!\n");
            return (1);
        }
        printf("%d ", i);
    }
    printf("\n");
    return (0);
}
