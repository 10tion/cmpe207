/*********************************************************/
/* dateproc.c - remote procedures; called by server stub */
/*********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <rpc/rpc.h>
#include <time.h>
#include "date.h"

long * bindate_1_svc(void* noused, struct svc_req* nousedxx) {
    static long timeval; /* must be static */
    timeval = time((long *) 0);
    return (&timeval);
}
