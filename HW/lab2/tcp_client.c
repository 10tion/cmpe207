#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void usage_manual(void);

int main(int argc, char* argv[]){

    if(argc != 4){
        usage_manual();
        exit(-1);
    }
    
    /* ******************************************
     * man gethostbyname():
     * if input is server name, return server ip;
     * if input is ip, no loopup, return ip
     * ******************************************
     */
    
    // prepare to write 
    int fd = open("rcv_file.txt", O_CREAT|O_WRONLY, S_IRWXU);
    if(fd < 0){
        printf("error to open file\n"); 
        exit(-1);
    }

    printf("server ip:%s, server port: %s, file name:%s\n", argv[1], argv[2], argv[3]);
    struct hostent* server_ip = gethostbyname(argv[1]);
    if(NULL == server_ip){
        perror("gethostbyname return NULL\n");
        exit(-1);
    }
    
    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(atoi(argv[2]));
    memcpy(&server_info.sin_addr, server_ip->h_addr, server_ip->h_length);
   
    // create client UDP socket
    int client_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(client_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }
    
    // connect to daytime server
    int ret = connect(client_soc, (struct sockaddr*)&server_info, sizeof(server_info));
    if(ret != 0){
        printf("Failed to connect to the file server, host:%s\n", (char*)server_ip);
        close(client_soc);
        exit(-1);
    }

    //unsigned int time;
    int num_bytes = write(client_soc, argv[3], strlen(argv[3]));
    if(num_bytes == -1){
        printf("Failed to send message to file server:%s\n", (char*)(server_ip->h_addr));
        close(client_soc);
        exit(-1);
    }

    char buf[1024*20] = {0};
    char* start = buf;
    
    num_bytes = read(client_soc, start, sizeof(buf));
    printf("rcv %d, line:%d\n", num_bytes, __LINE__);

    int rcv_num = write(fd, start, num_bytes); 
    if(num_bytes == -1 || rcv_num != num_bytes){
        printf("Failed to read message from file server:%s\n", (char*)(server_ip->h_addr));
        close(client_soc);
        exit(-1);
    }       

    while(num_bytes){
        start = start + num_bytes;

        num_bytes = read(client_soc, start, sizeof(buf));
        printf("rcv %d, line:%d\n", num_bytes, __LINE__);
        rcv_num = write(fd, start, num_bytes);
    }

    close(client_soc);
    
    return 0;    
}

/*
 * Out put manual
 * */
void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server IP -\n");
    printf("- server port number -\n");
    printf("- file name -\n");
}

