#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>

/* *
 * TCP server:
 * socket -> bind -> listen -> accept -> read -> write
 *
 * */
#define QUEUE_LEN 32
#define MAX_FILE_NAME_LEN   32
#define MAX_TRANSMIT_SIZE   512
#define MAX_FILE_SIZE       (1024*3)    //file size is 3K

void usage_manual(void);
void rcv_from_client(int);
void signal_child(int nouse);

int main(int argc, char* argv[]){

    // pass tcp listening port number 
    if(argc != 2){
        usage_manual();
        exit(-1);
    }   
    
    int port_num = atoi(argv[1]);
    printf("listening on port %d\n\n", port_num);

    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    server_info.sin_addr.s_addr = INADDR_ANY;
   
    // create server TCP socket
    int server_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(server_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }

    // bind the socket 
    int ret = bind(server_soc, (struct sockaddr *)&server_info, sizeof(server_info)); 
    if(ret != 0){
        printf("cannot bind to server:%d\n", port_num);
        close(server_soc);
        exit(-1);
    }

    // back up original handler for SIGCHLD
    void(* old_handler)(int);
    // wait for child avoid zombie
    old_handler = signal(SIGCHLD, signal_child);

    while(1){

        printf("waiting for sending file, ");

        // listen socket 
        ret = listen(server_soc, QUEUE_LEN);
        if(ret != 0){
            printf("cannot listen on %d\n", port_num);
            close(server_soc);
            exit(-1);
        }

        printf("listen socket number is:%d\n", server_soc);

        // accept connection
        struct sockaddr_in client_info;
        memset(&client_info, 0, sizeof(client_info));
        socklen_t client_addr_len = sizeof(client_info);
        
        int accept_soc = accept(server_soc, (struct sockaddr*)&client_info, &client_addr_len);
        if(accept_soc < 0){
            printf("accept failed, %s\n", strerror(errno));    
            close(server_soc);
            exit(-1);
        }

        printf("accept socket number is:%d, fork a new child process to handle\n", accept_soc);
        
        int child = fork();
        
        if(child == 0){ //this is child process
           printf("child pid %d\n", (int)getpid());
           rcv_from_client(accept_soc);           
           close(accept_soc);
           exit(0);
        }

        // this is parent process
        close(accept_soc);
    }

    // restore original handler for SIGCHLD
    signal(SIGCHLD, old_handler);
    return 0;
}

/*
 * Out put manual
 * */
void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server port number -\n");
}

void signal_child(int nouse){
    pid_t pid;
    int stat;
    while ((pid = waitpid(-1, &stat, WNOHANG)) > 0)
        printf("child_pid: %d dead\n", pid);
}

void rcv_from_client(int soc){
    char file_name[MAX_FILE_NAME_LEN] = {0};
    char file_content[MAX_FILE_SIZE] = {0};
    
    // get filename from client
    int num_bytes = read(soc, file_name, MAX_FILE_NAME_LEN);
    if(num_bytes < 0){
        printf("read failed\n");
        close(soc);
        exit(-1);
    }

    printf("reading file %s\n", file_name);

    // open file
    int fd = open(file_name, O_RDONLY);
    if(fd < 0){
        printf("error to open file: %s\n", file_name); 
        close(soc);
        exit(-1);
    }
    printf("fd is %d\n", fd);

    // read file content
    num_bytes = read(fd, file_content, MAX_FILE_SIZE);
    if(num_bytes < 0){
        printf("read failed\n");
        close(soc);
        exit(-1);
    }
    
    char* start = file_content;
    char* end = start + num_bytes - 1;

    // send file content to client
    int len = 0;
    while(start < end){
        if((end - start + 1) > MAX_TRANSMIT_SIZE){
            len = MAX_TRANSMIT_SIZE;    
        }else{
            len = end - start + 1;    
        }

        // write back to client
        printf("write %d to client\n", len);

        int send_num = write(soc, start, len); 
        if(send_num != len){
            printf("write error\n");
            close(fd);
            close(soc);
            exit(-1);
        }

        start += len;
    }

    printf("completed send\n");
    close(fd);
}

#if 0
void rcv_from_client(int soc){
    char buffer[MAX_TRANSMIT_SIZE] = {0};
    char file_name[MAX_FILE_NAME_LEN] = {0};
    
    sprintf(file_name, "%s_%d", "rcv_file", (int)getpid());

    // prepare to write 
    int fd = open(file_name,O_CREAT|O_WRONLY);
    if(fd < 0){
        printf("error to open file: %s\n", file_name); 
        close(soc);
        exit(-1);
    }
    printf("fd is %d\n", fd);

    // read from client
    int num_bytes = read(soc, buffer, sizeof(buffer));
    if(num_bytes < 0){
        printf("read failed\n");
        close(soc);
        exit(-1);
    }

    int count = 0; 
    while(num_bytes){   // only when client disconnected or number of requested bytes sent, the read return 0
        printf("rec %d of bytes\n", num_bytes);

        int rcv_num = write(fd, buffer, num_bytes); 
        if(rcv_num != num_bytes){
            printf("write error\n");
            close(fd);
            close(soc);
            exit(-1);
        }

        count += rcv_num;
        memset(buffer, 0, sizeof(buffer));
        printf("continue rec\n");
        num_bytes = read(soc, buffer, sizeof(buffer));
        if(num_bytes < 0){
            printf("read failed\n");
            close(fd);
            close(soc);
            exit(-1);
        }
    }

    printf("completed rec %d\n", count);

    close(fd);
}
#endif
