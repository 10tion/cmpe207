#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* *
 * UDP server:
 * socket -> bind -> recvfrom(block) -> send
 *
 * */
#define MAX_FILE_NAME_LEN   32
#define MAX_TRANSMIT_SIZE   512

// package encapsulation with sequence number
struct RecPkg {
    short seq;
    char buf[MAX_TRANSMIT_SIZE];
} data;

typedef struct{
    char filename[MAX_FILE_NAME_LEN];
    short seq;
} Req;

void usage_manual(void);

void check(void);

// argv[1] host, argv[2] port, argv[3] filename
int main(int argc, char* argv[]){

    // pass UDP listening port number
    if(argc != 4){
        usage_manual();
        exit(-1);
    }

    int port_num = atoi(argv[2]);
    printf("listening to port %d\n\n", port_num);

    struct hostent *host;
    struct sockaddr_in server_info;

    if((host=gethostbyname(argv[1]))==NULL){
        herror("gethostbyname error!");
        exit(1);
    }

    // create or open file in binary mode
    FILE *fp;
    printf("open file....\n");
    fp=fopen("savefile","wb");

    if(fp==NULL){
        perror("failed to create or open file");
        exit(1);
    }

    printf("connecting server....\n");

    server_info.sin_family=AF_INET;
    server_info.sin_port=htons(port_num);
    server_info.sin_addr = *((struct in_addr *)host->h_addr);

    int addr_len = sizeof(struct sockaddr_in);

    bzero(&(server_info.sin_zero),8);

    int sockfd=socket(AF_INET,SOCK_DGRAM,0);

    // initialize request
    Req request;
    
    request.seq = 0;

    strncpy(request.filename, argv[3], sizeof(argv[3]));

    int sendlen = sendto(sockfd, (char*)&request, sizeof(request), 0, (struct sockaddr*)&server_info, addr_len);

    if(sendlen == -1){
        printf("cannot request from server \n");
        close(sockfd);
        exit(-1);
    }

    printf("start receiving file \n");

    // receive packages based on sequence number, if package lost request the package again
    while(1){

        int len = recvfrom(sockfd, (char*)&data, sizeof(data), 0, (struct sockaddr*)&server_info, &addr_len);

        if(len> 0) {
            if(data.seq == request.seq) {
                fwrite(data.buf, sizeof(char), sizeof(data.buf), fp);
                request.seq++;
            }
        }
        
        else if(len == 0) {
            // receive empty package, terminate transmission
            fclose(fp);
            break;
        }

        else {
            printf("error!~\n");
        }

        sendlen = sendto(sockfd, (char*)&request, sizeof(request), 0, (struct sockaddr*)&server_info, addr_len);

        if(sendlen == -1){
            printf("cannot request from server");
            close(sockfd);
            exit(-1);
        }
    }

    check();

    close(sockfd);

    return 0;
}

void check(void) {
    FILE *fp;
    if((fp=fopen("savefile","rb"))==NULL) {
        printf("Error on receiving or saving \n");
    }
    else {
        printf("File saved \n");
    }
}

void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server port number -\n");
}
