#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

/* *
 * UDP server:
 * socket -> bind -> recv -> send
 *
 * */
#define QUEUE_LEN 32
#define MAX_FILE_NAME_LEN   32
#define MAX_TRANSMIT_SIZE   512
#define MAX_NUM_OF_THREAD   16

// package encapsulation with sequence number
typedef struct {
    short seq;
    char buf[MAX_TRANSMIT_SIZE];
} Pkg;

typedef struct {
    char filename[MAX_FILE_NAME_LEN];
    short seq;
} Req;

typedef struct {
    struct sockaddr_in from;
    Req request;
} Threadargs;

// pointer to files
Pkg *test1k = NULL;
Pkg *test2k = NULL;

int server_soc;
extern int errno;

void read_in_file(void);
void usage_manual(void);
void* process_with_client(void* targ);

int main(int argc, char* argv[]){

    // pass tcp listening port number
    if(argc != 2){
        usage_manual();
        exit(-1);
    }

    int port_num = atoi(argv[1]);
    printf("listening on port %d\n\n", port_num);

    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    server_info.sin_addr.s_addr = INADDR_ANY;

    // create server UDP socket
    server_soc = socket(PF_INET, SOCK_DGRAM, 0);
    if(server_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }

    // bind the socket
    int ret = bind(server_soc, (struct sockaddr *)&server_info, sizeof(server_info));
    if(ret != 0){
        printf("cannot bind to server:%d\n", port_num);
        close(server_soc);
        exit(-1);
    }

    // read in file
    test1k = (Pkg *) malloc(2 * sizeof(Pkg));
    test2k = (Pkg *) malloc(4 * sizeof(Pkg));

    read_in_file();

    pthread_t sniffer_thread[MAX_NUM_OF_THREAD] = {0};
    int thread_idx = 0;

    while(1){
        printf("waiting for incomming client, ");

        // accept connection
        struct sockaddr_in client_info;
        memset(&client_info, 0, sizeof(client_info));
        socklen_t client_addr_len = sizeof(client_info);


        Req request = {"", 0};

        Threadargs targ;

        int recvlen = recvfrom(server_soc, (char*)&request, sizeof(request), 0, (struct sockaddr*)&client_info, &client_addr_len);

        targ.request = request;
        targ.from = client_info;

        if(recvlen == -1) {
            printf("error on receive \n");
            continue;
        }

        // create thread to process incoming connection
        if(pthread_create(&sniffer_thread[thread_idx++], NULL, process_with_client, &targ) < 0){
            printf("could not create thread");
            exit(-1);
        }
    }

    return 0;
}

void* process_with_client(void* targ){

    Req request = ((Threadargs*)targ)->request;
    struct sockaddr_in from = ((Threadargs*)targ)->from;

    Pkg* package = NULL;

    // recognize request of file size from client
    char str1[] = "test1k";
    char str2[] = "test2k";
    int maxseq = 0;

    if(strncmp(request.filename, str1, sizeof(str1)) == 0) {
        package = test1k;
        maxseq = 1;
    }
    else if(strncmp(request.filename, str2, sizeof(str2)) == 0) {
        package = test2k;
        maxseq = 3;
    }
    else {
        printf("wrong args from client \n");
        close(server_soc);
        exit(-1);
    }

    
    // valid request
    if(request.seq <= maxseq) {
        int sendlen = sendto(server_soc, (char*)(&(package[request.seq])), sizeof(package[request.seq]), 0, (struct sockaddr*)&from, sizeof(from));

        if(sendlen == -1){
            printf("cannot send package \n");
            printf("errno %d\n", errno);
            exit(-1);
        }
    }

        // send a 0 size package
    else {
        int sendlen = sendto(server_soc, (char*)(&(package[maxseq])), 0, 0, (struct sockaddr*)&from, sizeof(from));

        if(sendlen == -1){
            printf("cannot send package \n");
            exit(-1);
        }
    }
}

/*
 * read test files in memory
 * */
void read_in_file(void){

    char read_buf[MAX_TRANSMIT_SIZE] = {0};

    FILE *fp = fopen("test1k", "rb");

    for(int i = 0; i < 2; i++) {
        memset(read_buf, 0, sizeof(read_buf));
        int rc = fread(read_buf, sizeof(char), sizeof(read_buf), fp);

        if(rc <= 0){
            printf("cannot read file \n");
            close(server_soc);
            exit(-1);
        }

        test1k[i].seq = i;
        memcpy(test1k[i].buf, read_buf, sizeof(test1k[i].buf));
    }

    fclose(fp);

    fp = fopen("test2k", "rb");

    for(int i = 0; i < 4; i++) {
        memset(read_buf, 0, sizeof(read_buf));
        int rc = fread(read_buf, sizeof(char), sizeof(read_buf), fp);

        if(rc <= 0){
            printf("cannot read file \n");
            close(server_soc);
            exit(-1);
        }

        test2k[i].seq = i;
        memcpy(test2k[i].buf, read_buf, sizeof(test2k[i].buf));
    }

    fclose(fp);
}

/*
 * Out put manual
 * */
void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server port number -\n");
}