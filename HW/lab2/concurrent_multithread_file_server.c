#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

/* *
 * TCP server:
 * socket -> bind -> listen -> accept -> read -> write
 *
 * */
#define QUEUE_LEN 32
#define MAX_FILE_NAME_LEN   32
#define MAX_FILE_SIZE       (1024*3)
#define MAX_TRANSMIT_SIZE   512
#define MAX_NUM_OF_THREAD   16

void usage_manual(void);
void* rcv_from_client(void* soc);

int main(int argc, char* argv[]){

    // pass tcp listening port number 
    if(argc != 2){
        usage_manual();
        exit(-1);
    }   
    
    int port_num = atoi(argv[1]);
    printf("listening on port %d\n\n", port_num);

    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    server_info.sin_addr.s_addr = INADDR_ANY;
   
    // create server TCP socket
    int server_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(server_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }

    // bind the socket 
    int ret = bind(server_soc, (struct sockaddr *)&server_info, sizeof(server_info)); 
    if(ret != 0){
        printf("cannot bind to server:%d\n", port_num);
        close(server_soc);
        exit(-1);
    }
     
    pthread_t sniffer_thread[MAX_NUM_OF_THREAD] = {0};
    int thread_idx = 0;

    while(1){
        printf("waiting for incomming client, ");

        // listen socket 
        ret = listen(server_soc, QUEUE_LEN);
        if(ret != 0){
            printf("cannot listen on %d\n", port_num);
            close(server_soc);
            exit(-1);
        }

        printf("listen socket number is:%d\n", server_soc);

        // accept connection
        struct sockaddr_in client_info;
        memset(&client_info, 0, sizeof(client_info));
        socklen_t client_addr_len = sizeof(client_info);
        
        int accept_soc = accept(server_soc, (struct sockaddr*)&client_info, &client_addr_len);
        if(accept_soc < 0){
            printf("accept failed, %s\n", strerror(errno));    
            close(server_soc);
            exit(-1);
        }
        printf("accept sock is:%d\n", accept_soc);
        // create thread to process incoming connection
        if(pthread_create(&sniffer_thread[thread_idx++], NULL, rcv_from_client, (void*)&accept_soc) < 0){
            printf("could not create thread");
            exit(-1);
        }
    }

    return 0;
}

/*
 * Out put manual
 * */
void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server port number -\n");
}

void* rcv_from_client(void* soc){

    char buffer[MAX_FILE_SIZE] = {0};
    char file_name[MAX_FILE_NAME_LEN] = {0};
    int accept_soc = *((int *)soc);
    
    printf("accept socket in thread:%d\n", accept_soc);
    
    // get filename from client
    int num_bytes = read(accept_soc, file_name, sizeof(file_name));
    if(num_bytes < 0){
        printf("read failed %s\n", strerror(errno));
        close(accept_soc);
        exit(-1);
    }

    printf("filename is %s\n", file_name);

    // prepare to write 
    int fd = open(file_name, O_RDONLY);
    if(fd < 0){
        printf("error to open file\n"); 
        close(accept_soc);
        exit(-1);
    }
    printf("fd is %d\n", fd);

    // read file content 
    num_bytes = read(fd, buffer, MAX_FILE_SIZE);
    if(num_bytes < 0){
        printf("read failed %s\n", strerror(errno));
        close(accept_soc);
        close(fd);
        exit(-1);
    }

    char* start = buffer;
    char* end = start + num_bytes - 1;
    
    // send file content to client
    int len = 0; 
    while(start < end){ 
        if((end - start + 1) > MAX_TRANSMIT_SIZE){
            len = MAX_TRANSMIT_SIZE;    
        }else{
            len = end - start + 1;    
        }
        
        // write back to client
        printf("write %d to client\n", len);

        int send_num = write(accept_soc, start, len); 
        if(send_num != len){
            printf("write error\n");
            close(accept_soc);
            close(fd);
            exit(-1);
        }
        
        start += len;
    }

    printf("completed send\n");
    
    //remember to close accept socket
    close(accept_soc);
    close(fd);
    return NULL;
}
