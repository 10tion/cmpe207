#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* *
 * UDP server:
 * socket -> bind -> recvfrom(block) -> send
 *
 * */
#define MAX_FILE_NAME_LEN   32
#define MAX_TRANSMIT_SIZE   512

// package encapsulation with sequence number
typedef struct {
    short seq;
    char buf[MAX_TRANSMIT_SIZE];
} Pkg;

typedef struct {
    char filename[MAX_FILE_NAME_LEN];
    short seq;
} Req;

Pkg *test1k;
Pkg *test2k;

int server_soc;
extern int errno;

void usage_manual(void);
void read_in_file(void);


int main(int argc, char* argv[]){

    // pass UDP listening port number
    if(argc != 2){
        usage_manual();
        exit(-1);
    }

    int port_num = atoi(argv[1]);
    printf("listening on port %d\n\n", port_num);

    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(port_num);
    server_info.sin_addr.s_addr = INADDR_ANY;

    // create server UDP socket
    server_soc = socket(PF_INET, SOCK_DGRAM, 0);
    if(server_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }

    // bind the socket
    int ret = bind(server_soc, (struct sockaddr *)&server_info, sizeof(server_info));
    if(ret != 0){
        printf("cannot bind to server:%d\n", port_num);
        close(server_soc);
        exit(-1);
    }

    // allocate memory for different file, read into memory
    test1k = (Pkg *) malloc(2 * sizeof(Pkg));
    test2k = (Pkg *) malloc(4 * sizeof(Pkg));

    read_in_file();

    Req request;

    while(1){

        struct sockaddr_in client_info;
        memset(&client_info, 0, sizeof(client_info));
        socklen_t client_addr_len = sizeof(client_info);

        Pkg* package = NULL;

        int recvlen = recvfrom(server_soc, (char*)&request, sizeof(request), 0, (struct sockaddr*)&client_info, &client_addr_len);

        if(recvlen == -1){
            printf("cannot listen on %d\n", port_num);
            close(server_soc);
            exit(-1);
        }

        // recognize request of file size from client
        char str1[] = "test1k";
        char str2[] = "test2k";
        int maxseq = 0;

        if(strncmp(request.filename, str1, sizeof(str1)) == 0) {
            package = test1k;
            maxseq = 1;
        }
        else if(strncmp(request.filename, str2, sizeof(str2)) == 0) {
            package = test2k;
            maxseq = 3;
        }
        else {
            printf("wrong args from client \n");
            close(server_soc);
            exit(-1);
        }


        // valid request
        if(request.seq <= maxseq) {
            int sendlen = sendto(server_soc, (char*)(&(package[request.seq])), sizeof(package[request.seq]), 0, (struct sockaddr*)&client_info, client_addr_len);

            if(sendlen == -1){
                printf("cannot send package \n");
                printf("errno = %d \n", errno);
                close(server_soc);
                exit(-1);
            }
        }

        // send a 0 size package if invalid request
        else {
            int sendlen = sendto(server_soc, (char*)(&(package[maxseq])), 0, 0, (struct sockaddr*)&client_info, client_addr_len);

            if(sendlen == -1){
                printf("cannot send package \n");
                printf("errno = %d \n", errno);
                close(server_soc);
                exit(-1);
            }
        }
    }

    free(test1k);
    free(test2k);

    return 0;
}

void usage_manual(void){
    printf("- please provide following parameters in order -\n");
    printf("- server port number -\n");
}

void read_in_file(void){

    char read_buf[MAX_TRANSMIT_SIZE];

    FILE *fp = fopen("test1k", "rb");

    for(int i = 0; i < 2; i++) {
        memset(read_buf, 0, sizeof(read_buf));
        int rc = fread(read_buf, sizeof(char), sizeof(read_buf), fp);

        if(rc <= 0){
            printf("cannot read file \n");
            close(server_soc);
            exit(-1);
        }

        test1k[i].seq = i;
        memcpy(test1k[i].buf, read_buf, sizeof(test1k[i].buf));
    }

    fclose(fp);

    fp = fopen("test2k", "rb");

    for(int i = 0; i < 4; i++) {
        memset(read_buf, 0, sizeof(read_buf));
        int rc = fread(read_buf, sizeof(char), sizeof(read_buf), fp);

        if(rc <= 0){
            printf("cannot read file \n");
            close(server_soc);
            exit(-1);
        }

        test2k[i].seq = i;
        memcpy(test2k[i].buf, read_buf, sizeof(test2k[i].buf));
    }

    fclose(fp);
}