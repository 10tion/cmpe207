typedef enum result {
    SUCCESS,
    FAIL
} result_t;

typedef enum client_who {   // ONLY used in client side
    CUSTOMER,
    EMPLOYEE,
    ADMIN
} client_who_t;

typedef enum action {
    VERIFY,
    WITHDRAW,
    DEPOSIT,
    TRANSFER,
    BALANCE,
    HISTORY,
    MODIFYPW,
    C_CUSTOMER,    //create customer name
    D_CUSTOMER,    //delete customer name
    C_CUSTOMER_ACCOUNT,
    D_CUSTOMER_ACCOUNT,
    C_EMPLOYEE_ACCOUNT,
    D_EMPLOYEE_ACCOUNT,
    MODIFY_ADMIN_PW,
    EXIT
} action_t;

#if 0
typedef struct account {
    const char* username;
    const char* password;
} account_t;

typedef struct client_info { // ONLY used in client side
    client_who_t who;
    int client_sock;
} client_info_t;

typedef struct server_info {
    int server_sock;
} server_info_t;
#endif
