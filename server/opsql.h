#ifndef _OPSQL_H
#define _OPSQL_H
#include <string.h>
#include <stdbool.h>

//extern int func1(void);
extern int authenticate(char* name, char* passwd);
extern int authenticate_teller_admin (char* name, char* passwd, bool teller);

extern int customer_logout(char* username);

extern double common_call(char* name, char* account, char* recv_account, double amount, int query_row_num, int sock, int action);

extern int transfer(char* sender, char* sender_account, char* recver_account, double amt);
extern int withdraw(char* name, char* account, double amt);
extern int deposit(char* name, char* account, double amt);
extern int query_history(char* name, char* account, int num, int sock);
extern double query_balance(char* name, char* account);
extern int change_passwd(char* name, char* ssn, char* new_passwd);

extern int create_customer(char* username, char* passwd, char* ssn);
extern int delete_customer(char* username);
extern int create_account(char* username, char* account);
extern int delete_account(char* account);

extern int create_teller(char* teller, char* passwd);
extern int delete_teller(char* teller);
extern int change_teller_passwd(char* teller, char* passwd);
extern int change_admin_passwd(char* adminname, char* passwd);


#endif
