#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <mysql/mysql.h>
#include <errno.h>
#include <stdbool.h>

#include "cJSON.h"
#include "opsql.h"
#include "msg_typedef.h"

#define MAX_BUF_SIZE 1024
#define MAX_USER_LEN 16
#define MAX_ACCOUNT_LEN 32
#define MAX_MSG_LEN 128

// login info of databse

void msg_handler(int sock, char* errmsg, double res);


void teller_handler(int sock, cJSON *root) {

    cJSON *msg_recv;

    char buf[MAX_BUF_SIZE] = {0};

    cJSON *root_name = cJSON_GetObjectItemCaseSensitive(root, "teller name");
    cJSON *root_password = cJSON_GetObjectItemCaseSensitive(root, "teller password");

    char tellername[MAX_USER_LEN] = {0};
    char password[MAX_USER_LEN] = {0};

    char errmsg[MAX_MSG_LEN] = {0};


    if(cJSON_IsString(root_name)) {
        strncpy(tellername, (char *) root_name->valuestring, strlen((char *) root_name->valuestring));
    } else {
        printf(" invalid format of username in cJSON package \n");
        return;
    }

    if(cJSON_IsString(root_password)) {
        strncpy(password, (char *) root_password->valuestring, strlen((char *) root_password->valuestring));
    } else {
        printf(" invalid format of password in cJSON package \n");
        return;
    }


    int authen = authenticate_teller_admin (tellername, password, 1);
    memset(errmsg, 0, MAX_MSG_LEN);

    if(authen) {
        msg_handler(sock, "error !\n", 1.0);
        return;
    }

    msg_handler(sock, "success in login!\n", 0.0);



    int errcount = 0;

    while(1) {

        int buf_len = read(sock, buf, MAX_BUF_SIZE);

        if(buf_len <= 0) {

            if(errno != EINTR) {
                printf("CLient disconnected! Close!\n");
                return;
            }

            // modify to write into log file
            printf("error to read from clients \n");
            errcount++;

            if(errcount >= 10) return;
            continue;
        } else {
            errcount = 0;
        }

        char* buf_ptr = buf;

        msg_recv = cJSON_Parse(buf_ptr);

        //cJSON *msg_send = cJSON_CreateObject();
        //char buf_send[MAX_BUF_SIZE] = {0};

        if(msg_recv == NULL) {
            printf(" cannot parse received package \n");
            errcount++;

            if(errcount >= 10) {
                return;
            }
                continue;
        } else {
            errcount = 0;
        }


        cJSON *action = cJSON_GetObjectItemCaseSensitive(msg_recv, "action");

        if(!cJSON_IsNumber(action)) {
            printf(" invalid action type! \n");
            continue;
        }

        int action_num = (int) action->valuedouble;
        //printf("action: %d\n",(int) action->valuedouble);

        switch(action_num) {

            // transfer
            case 3: {
                char username[MAX_ACCOUNT_LEN] = {0};
                char transfer_account[MAX_ACCOUNT_LEN] = {0};
                char account_name[MAX_ACCOUNT_LEN] = {0};
                //char password[MAX_ACCOUNT_LEN] = {0};
                double amount = 0.0;

                cJSON *root_username = cJSON_GetObjectItemCaseSensitive(msg_recv, "name");
                //cJSON *root_password = cJSON_GetObjectItemCaseSensitive(msg_recv, "password");
                cJSON *root_user_account = cJSON_GetObjectItemCaseSensitive(msg_recv, "from account");
                cJSON *root_transfer_account = cJSON_GetObjectItemCaseSensitive(msg_recv, "to account");
                cJSON *root_amount = cJSON_GetObjectItemCaseSensitive(msg_recv, "amount");

                strncpy(account_name,
                            (char *) root_user_account->valuestring,
                            strlen((char *) root_user_account->valuestring));

                strncpy(username,
                            (char *) root_username->valuestring,
                            strlen((char *) root_username->valuestring));

                amount = root_amount->valuedouble;

                if(cJSON_IsString(root_transfer_account)) {
                    strncpy(transfer_account,
                            (char *) root_transfer_account->valuestring,
                            strlen((char *) root_transfer_account->valuestring));
                } else {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    strcpy(errmsg, " Cannot fetch valid transfer account name! \n");

                    msg_handler(sock, "invalid transfer account\n", 1.0);
                    continue;
                }

                int res = transfer(username, account_name, transfer_account, amount);
                if(!res) {
                    msg_handler(sock, "success! \n", 0.0);
                    printf("transfer from account %s to account %s, amount: %f\n", account_name, transfer_account, amount);
                    continue;
                } else {
                    if(res == -1) {
                        msg_handler(sock, "not enough balance! \n", 1.0);
                        continue;
                    } else if(res == -5) {
                        msg_handler(sock, "database! \n", 1.0);
                        continue;
                    }
                }

            }



            // change customer password
            case 6: {
                cJSON *root_username = cJSON_GetObjectItemCaseSensitive(msg_recv, "name");
                cJSON *root_ssn = cJSON_GetObjectItemCaseSensitive(msg_recv, "ssn");
                cJSON *root_newpasswd = cJSON_GetObjectItemCaseSensitive(msg_recv, "new password");

                char username[MAX_ACCOUNT_LEN] = {0};
                char ssn[MAX_ACCOUNT_LEN] = {0};
                char newpasswd[MAX_ACCOUNT_LEN] = {0};

                strncpy(username, (char *) root_username->valuestring, strlen((char *) root_username->valuestring));
                strncpy(ssn, (char *) root_ssn->valuestring, strlen((char *) root_ssn->valuestring));
                strncpy(newpasswd, (char *) root_newpasswd->valuestring, strlen((char*) root_newpasswd->valuestring));

                //printf(" %s %s %s ", username, newpasswd, ssn);
                int ret = change_passwd(username, ssn, newpasswd);

                if(!ret) {
                    msg_handler(sock, "passwd changed! \n", 0.0);
                    printf("password of customer %s is changed, new password: %s\n", username, newpasswd);
                    continue;
                } else {
                    msg_handler(sock, "error occured in change! \n", 1.0);
                    continue;
                }
            }

            // create customer
            case 7: {
                cJSON *customer_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "customer name");
                cJSON *customer_passwd = cJSON_GetObjectItemCaseSensitive(msg_recv, "customer password");
                cJSON *customer_ssn = cJSON_GetObjectItemCaseSensitive(msg_recv, "ssn");

                char username[MAX_USER_LEN] = {0};
                char password[MAX_USER_LEN] = {0};
                char ssn[MAX_USER_LEN] = {0};

                strncpy(username, (char *) customer_name->valuestring, strlen((char *) customer_name->valuestring));
                strncpy(password, (char *) customer_passwd->valuestring, strlen((char *) customer_passwd->valuestring));
                strncpy(ssn, (char *) customer_ssn->valuestring, strlen((char *) customer_ssn->valuestring));

                //printf("%s %s %s\n", username, password, ssn);
                int ret = create_customer(username, password, ssn);

                if(!ret) {
                    msg_handler(sock, "customer created in success!\n", 0.0);
                    printf("create new customer %s, password %s\n", username, password);
                else msg_handler(sock, "Error when creating customer\n", 1.0);

                break;
            }

            // delete customer
            case 8: {
                cJSON *customer_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "customer name");

                char username[MAX_USER_LEN] = {0};

                strncpy(username, (char *) customer_name->valuestring, strlen((char *) customer_name->valuestring));

                int ret = delete_customer(username);

                if(!ret) {
                    msg_handler(sock, "customer deleted in success!\n", 0.0);
                    printf("customer %s information deleted \n", username);
                }
                else msg_handler(sock, "Error when deleting customer\n", 1.0);


                break;
            }

            // create account
            case 9: {
                cJSON *customer_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "customer name");
                cJSON *account_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "account name");

                char username[MAX_USER_LEN] = {0};
                char accountname[MAX_USER_LEN] = {0};

                strncpy(username, (char *) customer_name->valuestring, strlen((char *) customer_name->valuestring));
                strncpy(accountname, (char *) account_name->valuestring, strlen((char *) account_name->valuestring));

                int ret = create_account(username, accountname);

                if(!ret) {
                    msg_handler(sock, "account created in success!\n", 0.0);
                    printf("new account %s of customer %s created! \n", accountname, username);
                }
                else msg_handler(sock, "Error when creating account\n", 1.0);


                break;
            }

            // delete account
            case 10: {
                cJSON *account_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "account name");

                char accountname[MAX_USER_LEN] = {0};

                strncpy(accountname, (char *) account_name->valuestring, strlen((char *) account_name->valuestring));

                int ret = delete_account(accountname);

                if(!ret) {
                    msg_handler(sock, "account deleted in success!\n", 0.0);
                    printf("account %s deleted! \n", accountname);
                }
                else msg_handler(sock, "Error when deleting account\n", 1.0);


                break;
            }

            case 14: {
                         printf("teller %s logout!\n", tellername);
                         msg_handler(sock, "log out! \n", 0.0);
                         cJSON_Delete(msg_recv);
                         return;
                     }

            default: {
                msg_handler(sock, "Invalid action received!\n", 1.0);
                break;
            }
        }
    }
}


void msg_handler(int sock, char* errmsg, double res) {
    char *buf = NULL;

    cJSON *err = cJSON_CreateObject();

    cJSON_AddNumberToObject(err, "result", res);
    cJSON_AddItemToObject(err, "message", cJSON_CreateString(errmsg));

    buf = cJSON_PrintBuffered(err, 1, 1);
    int send_len = write(sock, buf, MAX_BUF_SIZE);

    if(send_len < 0) {
        printf("send error! \n");
    }
    cJSON_Delete(err);
}


