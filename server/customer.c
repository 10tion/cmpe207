#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <mysql/mysql.h>
#include "opsql.h"

#include "cJSON.h"
#include "msg_typedef.h"

#define MAX_BUF_SIZE 1024
#define MAX_USER_LEN 16
#define MAX_ACCOUNT_LEN 32
#define MAX_MSG_LEN 128



void customer_handler(int sock, cJSON *root);
void error_handler(int sock, char* errmsg);
static void success_msg(int sock, char* message);

// login info of databse
char *server = "localhost";
char *sqluser = "root";
char *password = "1206";
char *database = "mysql";

void customer_handler(int sock, cJSON *root) {
    // login check
    // if condition variable == 1 send error msg, return;
    // else set condition variable = 1, send success msg;

    char buf[MAX_BUF_SIZE] = {0};

    cJSON *root_name = cJSON_GetObjectItemCaseSensitive(root, "customer name");
    cJSON *root_password = cJSON_GetObjectItemCaseSensitive(root, "customer password");

    char username[MAX_USER_LEN] = {0};
    char password[MAX_USER_LEN] = {0};

    char errmsg[MAX_MSG_LEN] = {0};


    if(cJSON_IsString(root_name)) {
        strncpy(username, (char *) root_name->valuestring, strlen((char *) root_name->valuestring));
    } else {
        printf(" invalid format of username in cJSON package \n");
        return;
    }

    if(cJSON_IsString(root_password)) {
        strncpy(password, (char *) root_password->valuestring, strlen((char *) root_password->valuestring));
    } else {
        printf(" invalid format of password in cJSON package \n");
        return;
    }


    int authen = authenticate(username, password);
    memset(errmsg, 0, MAX_MSG_LEN);

    switch(authen) {
        case -1: {
            printf("error 1 \n");
            strcpy(errmsg, " No User Information in Database! \n");
            error_handler(sock, errmsg);
            return;
        }
        case -2: {
            printf("error 2 \n");
            strcpy(errmsg, " Invalid Password! \n");
            error_handler(sock, errmsg);
            return;
        }
        case -3: {
            printf("error 3 \n");
            strcpy(errmsg, " Multi login, you have logged in from another client!\n");
            error_handler(sock, errmsg);
            return;
        }
        case -4: {
            printf("error 4 \n");
            strcpy(errmsg, " Duplicate user info in database, please contact teller!\n");
            error_handler(sock, errmsg);
            return;
        }
        case -5: {
            printf("error 5 \n");
            strcpy(errmsg, " Database error!\n");
            error_handler(sock, errmsg);
            return;
        }
        default: {
            printf("LOGIN! \n");
            // display success info
            strcpy(errmsg, " LOGIN! \n");
            success_msg(sock, errmsg);
        }
    }

    // Login, operation loop
    int errcount = 0;

    while(1) {

        int buf_len = read(sock, buf, MAX_BUF_SIZE);


        if(buf_len <= 0) {
            if(errno != EINTR) {
                printf("CLient disconnected! Close!\n");
                return;
            }
            // modify to write into log file
            printf("error to read from clients \n");
            errcount++;

            if(errcount >= 10) return;
            continue;
        } else {
            errcount = 0;
        }

        char *buf_ptr = buf;

        cJSON *msg_recv = cJSON_Parse(buf_ptr);

        /*
        for(int i = 0; i < strlen(buf_ptr); i++) {
            printf("%c", buf_ptr[i]);
        }
        printf("recv: %s", buf_ptr);
        */

        //cJSON *msg_send = cJSON_CreateObject();
        //char buf_send[MAX_BUF_SIZE] = {0};

        if(msg_recv == NULL) {
            printf(" cannot parse received package \n");
            errcount++;

            if(errcount >= 10) {
                customer_logout(username);
                return;
            }
            continue;
        } else {
            errcount = 0;
        }


        cJSON *action = cJSON_GetObjectItemCaseSensitive(msg_recv, "action");

        if(!cJSON_IsNumber(action)) {
            printf(" invalid action type! \n");
            continue;
        }

        int action_num = (int) action->valuedouble;
        //printf("action: %d\n",(int) action->valuedouble);

        // most action related to account operation, fetch account first
        char account_name[MAX_ACCOUNT_LEN] = {0};
        if(action_num <= 5) {
        cJSON *root_account = cJSON_GetObjectItemCaseSensitive(msg_recv, "account");

        if(cJSON_IsString(root_account)) {

            strncpy(account_name, (char *) root_account->valuestring, strlen((char *) root_account->valuestring));
            printf("account name: %s\n", account_name);

        } else {
            memset(errmsg, 0, MAX_MSG_LEN);
            strcpy(errmsg, " Cannot fetch valid account name! \n");

            error_handler(sock, errmsg);
            continue;
        }
        }

        // cJSON_free(root_account);

        // get amount for later operation
        double amount = 0.0;
        if(action_num <= 3) {
            cJSON *root_amount = cJSON_GetObjectItemCaseSensitive(msg_recv, "amount");
            //if(root_amount == NULL) printf("root amount null \n");
            //else printf("root amount %f \n", root_amount->valuedouble);


            if(cJSON_IsNumber(root_amount)) {
                amount = root_amount->valuedouble;
                printf(" is number \n");
            } else {
                memset(errmsg, 0, MAX_MSG_LEN);
                strcpy(errmsg, " invalid amount number! \n");
                printf("is not number \n");

                error_handler(sock, errmsg);
                continue;
            }
        }

        //cJSON_free(root_amount);

        //int client_action = (int) action->valuedouble;
        //printf("test point 1 \n");
        //printf("action number: %d \n", action_num);

        // switch action
        switch(action_num) {

            // withdraw
            case 1: {
                //printf("test point 3\n");
                int res = withdraw(username, account_name, amount);
                printf("res of withdraw %d\n", res);
                if(!res) {
                    printf("test point 4\n");
                    memset(errmsg, 0, MAX_MSG_LEN);
                    strcpy(errmsg, "withdraw succeed! \n");
                    success_msg(sock, errmsg);
                    printf("customer %s withdrawed %f from account %s\n", username, amount, account_name);
                    continue;
                } else {
                    if(res == -1) {
                        printf("Not enough balance\n");
                        memset(errmsg, 0, MAX_MSG_LEN);
                        strcpy(errmsg, " invalid amount number! \n");

                        error_handler(sock, errmsg);
                        continue;
                    } else if(res == -5) {
                        printf("database error\n");
                        memset(errmsg, 0, MAX_MSG_LEN);
                        strcpy(errmsg, " database error! \n");

                        error_handler(sock, errmsg);
                        continue;
                    }
                }
            }

            // deposit
            case 2: {
                int res = deposit(username, account_name, amount);
                if(!res) {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    strcpy(errmsg, "deposit succeed! \n");
                    printf("customer %s deposit %f to account %s", username, amount, account_name);
                    success_msg(sock, errmsg);
                    continue;
                } else {
                    if(-5 == res) {
                        memset(errmsg, 0, MAX_MSG_LEN);
                        strcpy(errmsg, " database error! \n");

                        error_handler(sock, errmsg);
                        continue;
                    }
                }
            }

            // transfer
            case 3: {
                char transfer_account[MAX_ACCOUNT_LEN] = {0};
                cJSON *root_transfer_account = cJSON_GetObjectItemCaseSensitive(msg_recv, "transfer account name");

                if(cJSON_IsString(root_transfer_account)) {
                    strncpy(transfer_account,
                            (char *) root_transfer_account->valuestring,
                            strlen((char *) root_transfer_account->valuestring));
                } else {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    strcpy(errmsg, " Cannot fetch valid transfer account name! \n");

                    error_handler(sock, errmsg);
                    continue;
                }

                int res = transfer(username, account_name, transfer_account, amount);
                if(!res) {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    strcpy(errmsg, "transfer succeed! \n");
                    success_msg(sock, errmsg);
                    printf("customer %s transfer %f from his/her account %s to account %s\n", username, amount, account_name, transfer_account);
                    continue;
                } else {
                    if(res == -1) {
                        memset(errmsg, 0, MAX_MSG_LEN);
                        strcpy(errmsg, " sender not enough balance! \n");

                        error_handler(sock, errmsg);
                        continue;
                    } else if(res == -5) {
                        memset(errmsg, 0, MAX_MSG_LEN);
                        strcpy(errmsg, " database error! \n");

                        error_handler(sock, errmsg);
                        continue;
                    }
                }

                cJSON_Delete(root_transfer_account);
            }

            // query balance
            case 4: {
                double balance = query_balance(username, account_name);
                if(balance >= 0) {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    sprintf(errmsg, "balance is %f\n", balance);
                    printf("balance of account %s is %f\n", account_name, balance);
                    success_msg(sock, errmsg);
                    continue;
                } else {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    strcpy(errmsg, " database error! \n");

                    error_handler(sock, errmsg);
                    continue;
                }
            }

            // query history
            case 5: {
                        cJSON *root_num_of_entries = cJSON_GetObjectItemCaseSensitive(msg_recv, "num_of_entries");
                        int num_of_entries = -1;

                        num_of_entries = (int) root_num_of_entries->valuedouble;

                        int ret = query_history(username, account_name, num_of_entries, sock);

                        break;
                        /*
                        if(!ret) {
                            success_msg(sock, "query done! \n");
                            continue;
                        } else {
                            error_handler(sock, "no history \n");
                            continue;
                        }
                        */
                    }

            // change password
            case 6: {
                cJSON *root_ssn = cJSON_GetObjectItemCaseSensitive(msg_recv, "ssn");
                cJSON *root_newpasswd = cJSON_GetObjectItemCaseSensitive(msg_recv, "new password");

                char ssn[MAX_ACCOUNT_LEN] = {0};
                char newpasswd[MAX_ACCOUNT_LEN] = {0};

                strncpy(ssn, (char *) root_ssn->valuestring, strlen((char *) root_ssn->valuestring));
                strncpy(newpasswd, (char *) root_newpasswd->valuestring, strlen((char*) root_newpasswd->valuestring));

                int ret = change_passwd(username, ssn, newpasswd);

                if(!ret) {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    success_msg(sock, "passwd changed! \n");
                    printf("password of customer %s changed, new password: %s \n", username, newpasswd);
                    continue;
                } else {
                    memset(errmsg, 0, MAX_MSG_LEN);
                    error_handler(sock, "error occured in change! \n");
                    continue;
                }
            }


            // exit
            case 14: {
                if(customer_logout(username)) {
                    printf("error in logout!");
                    error_handler(sock, errmsg);
                    continue;
                }

                printf("customer %s log out!\n", username);
                success_msg(sock, "Log out in success!");
                return;
            }

            default: {
                memset(errmsg, 0, MAX_MSG_LEN);
                strcpy(errmsg, " invalid action! \n");

                error_handler(sock, errmsg);
                continue;
            }
        }

        //cJSON_free(msg_recv);

        //cJSON_free(root_amount);
    }
}

void error_handler(int sock, char* errmsg) {
    char *buf = NULL;

    cJSON *err = cJSON_CreateObject();
    double res = 1.0;

    cJSON_AddNumberToObject(err, "result", res);
    cJSON_AddItemToObject(err, "message", cJSON_CreateString(errmsg));

    buf = cJSON_PrintBuffered(err, 1, 1);
    int send_len = write(sock, buf, MAX_BUF_SIZE);

    if(send_len < 0) {
        printf("send error! \n");
    }
    cJSON_Delete(err);
}

static void success_msg(int sock, char* message) {
    char *buf = NULL;

    cJSON *msg = cJSON_CreateObject();
    double res = 0.0;

    cJSON_AddNumberToObject(msg, "result", res);
    cJSON_AddItemToObject(msg, "message", cJSON_CreateString(message));

    buf = cJSON_PrintBuffered(msg, 1, 1);
    int send_len = write(sock, buf, MAX_BUF_SIZE);

    if(send_len < 0) {
        printf("send error! \n");
    }
    cJSON_Delete(msg);
}
