#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <mysql/mysql.h>
#include <stdbool.h>
#include "cJSON.h"

#define MAX_PACKAGE_SIZE 1024
#define MAX_MSG_LEN 256
#define MAX_SQL_LEN 256
#define MAX_FORM_LEN 64

// login info of databse
static char *server = "localhost";
static char *sqluser = "root";
static char *password = "1206";
//char *database = "mysql";

/******************************************************************
 * MYSQL_RES structure
 *
 * typedef struct st_mysql_res {
 *
 *  my_ulonglong row_count;
 *  unsigned int  field_count, current_field;
 *  MYSQL_FIELD   *fields;
 *  MYSQL_DATA    *data;
 *  MYSQL_ROWS    *data_cursor;
 *  MEM_ROOT      field_alloc;
 *  MYSQL_ROW     row;            If unbuffered read
 *  MYSQL_ROW     current_row;    buffer to current row
 *  unsigned long *lengths;       column lengths of current row
 *  MYSQL         *handle;        for unbuffered reads
 *  my_bool       eof;            Used my mysql_fetch_row
 *
 * } MYSQL_RES;
 *
 ********************************************************************
 */

// initialize of mysql login variables
MYSQL *conn;
MYSQL_RES *res;
MYSQL_ROW row;

double common_call(char* name, char* account, char* recv_account, double amt, int query_row_num, int sock, int action);
int withdraw_inner(double current_balance, double amt, MYSQL* conn2, char* form_name);
int deposit_inner(double current_balance, double amt, MYSQL* conn2, char* form_name);
int query_history_inner(MYSQL *conn2, char* form_name, int query_row_num, int sock);
int transfer_inner(double current_balance, double amt, MYSQL* conn2, char* form_name, char* recv_account);


/*******************************************
 * return value:
 *  0: authentication in success
 * -1: no such user
 * -2: invalid password
 * -3: multi login
 * -4: duplicate user info in customer table
 * -5: mysql read mysql_error
 *
 * *******************************************/

int authenticate_teller_admin (char* name, char* passwd, bool teller) {
    conn = mysql_init(NULL);
    char *database = "userinfo";

    char role[10] = {0};

    if(teller) strcpy(role, "teller");
    else strcpy(role, "admini");

    //printf("%s \n", name);
    //printf("%s \n", passwd);

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        //printf("-1");
        //exit(1);
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT password from %s where username = '%s';", role, name);

    //printf("%s \n", sqlcmd);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("-2");
        //exit(1);
    }

    res = mysql_store_result(conn);

    int row_num = mysql_num_rows(res);
    //printf("%d \n", row_num);

    int ret = -3;

    if(row_num != 1) {
        printf(" error occurs, error id: 1 \n");
        mysql_close(conn);
        return -1;
    }

    if((row = mysql_fetch_row(res)) == NULL) {
        printf("error in fetch rows \n");
        mysql_close(conn);
        return -2;
    }

    else if(!strncmp(row[0], passwd, strlen(passwd))) {
        printf("authenticat done! \n");
        ret = 0;
    }

    mysql_free_result(res);
    mysql_close(conn);

    return ret;
}

// authen customer
int authenticate(char* name, char* passwd) {
    conn = mysql_init(NULL);
    char *database = "userinfo";

    //printf("%s \n", name);
    //printf("%s \n", passwd);

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("-1");
        //exit(1);
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT password, lockstate from customer where username = '%s' for update;", name);

    //printf("%s \n", sqlcmd);


    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("-2");
        //exit(1);
    }

    res = mysql_store_result(conn);

    int row_num = mysql_num_rows(res);

    //printf("%d \n", row_num);

    // row array: password, lockstate
    if((row = mysql_fetch_row(res)) == NULL) {

        fprintf(stderr, "%s\n", mysql_error(conn));
        // exit(-1);
        printf("-3");
        return -5;
    }

    int ret = 0;
    switch(row_num) {
        case 0: {
            // no user info
            ret = -1;
            break;
        }
        case 1: {
            if(strcmp(row[0], passwd)) {
                // invalid passwd
                ret = -2;
                break;
            }

            if(!strcmp(row[1], "0")) {
                // unlock state, login available;
                mysql_free_result(res);
                memset(sqlcmd, 0, MAX_SQL_LEN);

                sprintf(sqlcmd, "UPDATE customer SET lockstate = '1' where username = '%s';", name);
                if (mysql_query(conn, sqlcmd)) {
                    fprintf(stderr, "%s\n", mysql_error(conn));
                    //exit(1);
                    return -5;
                } else {
                    return 0;
                }

            } else {
                // multi client login for sigle user
                return -3;
            }
        }
        default: {
            // row > 1; duplicate userinfo
            ret = -4;
            break;
        }
    }

    // ensure unlock row_lock
    mysql_query(conn, "commit work;");

    mysql_free_result(res);
    mysql_close(conn);

    return ret;
}

int customer_logout(char* username) {
    conn = mysql_init(NULL);
    char *database = "userinfo";

    //printf("%s \n", username);

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error connect when log out! ERR ID: 1");
        return -1;
        //exit(1);
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "UPDATE customer SET lockstate = '0' where username = '%s';", username);

    //printf("%s \n", sqlcmd);


    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error connect when log out! ERR ID: 2");
        //exit(1);
        return -1;
    }

    return 0;
}

int withdraw(char* name, char* account, double amt) {
	//printf(" enter! \n");
    return common_call(name, account, NULL, amt, 0, -1, 0);
}

int deposit(char* name, char* account, double amt) {
    return common_call(name, account, NULL, amt, 0, -1, 1);

}

double query_balance(char* name, char* account) {
    return common_call(name, account, NULL, 0, 0, -1, 2);
}

int query_history(char* name, char* account, int num, int sock) {
    return common_call(name, account, NULL, 0, num, sock, 3);
}


int transfer(char* sender, char* sender_account, char* recver_account, double amt) {

    return common_call(sender, sender_account, recver_account, amt, 0, -1, 4);
}

int change_passwd(char* name, char* ssn, char* new_passwd){
    conn = mysql_init(NULL);
    char *database = "userinfo";

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT SSN from customer where username = '%s';", name);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }

    res = mysql_store_result(conn);

    // get user UID
    if((row = mysql_fetch_row(res)) == NULL) {

        printf(" Cannot fetch customer information! \n");
        return -1;
    }

    if(mysql_num_rows(res) != 1) {
        // mysql_error
        return -1;
    }

    if(strncmp(row[0], ssn, strlen(ssn))) {
        printf(" ssn not matched! \n");
        return -1;
    }

    // get transaction form name
    mysql_free_result(res);
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "UPDATE customer SET password = '%s' where username = '%s';", new_passwd, name);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -2;
        //exit(1);
    }
    return 0;
}

int create_customer(char* username, char* passwd, char* ssn) {
    conn = mysql_init(NULL);
    char *database = "userinfo";
    //printf("enter creator customer func \n");

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT * from customer where username = '%s';", username);
    //printf("%s \n", sqlcmd);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }

    res = mysql_store_result(conn);

    if(mysql_num_rows(res)) {
        printf("exist customer! \n");
        mysql_close(conn);
        return -1;
    }

    mysql_free_result(res);
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd,
        "INSERT INTO customer (username, password, SSN) VALUES ('%s', '%s', '%s');",
        username, passwd, ssn);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in creating customer! \n");
        mysql_close(conn);
        return -2;
        //exit(1);
    }
    mysql_close(conn);
    return 0;
}

int delete_customer(char* username) {
    conn = mysql_init(NULL);
    char *database = "userinfo";

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT lockstate from customer where username = '%s';", username);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        return -1;
    }

    res = mysql_store_result(conn);

    if((row = mysql_fetch_row(res)) == NULL) {
        printf(" Cannot fetch customer information! \n");
        mysql_close(conn);
        return -1;
    }

    if(strncmp(row[0], "0", 1)) {
        printf("customer is login! \n");
        mysql_close(conn);
        return -1;
    }

    mysql_free_result(res);
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "DELETE FROM customer WHERE username = '%s';", username);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in deleting customer! \n");
        mysql_close(conn);
        return -2;
        //exit(1);
    }

    mysql_close(conn);
    return 0;
}

int create_account(char* username, char* account) {
    conn = mysql_init(NULL);
    char *database = "userinfo";
    //printf("11\n");
    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }
    //printf("22\n");

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT UID from customer where username = '%s';", username);

    //printf("32\n");
    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        return -1;
    }

    res = mysql_store_result(conn);

    if((row = mysql_fetch_row(res)) == NULL) {
        printf(" Cannot fetch customer information! \n");
        mysql_close(conn);
        return -1;
    }

    int uid = atoi(row[0]);

    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "SELECT * FROM account WHERE UID = %d AND account = '%s';", uid, account);


    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in query account table!\n");
        mysql_close(conn);
        return -2;
        //exit(1);
    }

    mysql_free_result(res);
    res = mysql_store_result(conn);

    if(mysql_num_rows(res)) {
        printf("account name should be unique! \n");
        mysql_close(conn);
        return -3;
    }

    char formname[MAX_FORM_LEN] = {0};
    sprintf(formname, "%s_%s", username, account);

    //printf("formname: %s \n", formname);


    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd,
            "INSERT INTO account (UID, account, transaction_form) VALUES (%d, '%s', '%s');",
             uid, account, formname);

    //printf("sqlcmd: %s \n", sqlcmd);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in query account table!\n");
        mysql_close(conn);
        return -2;
        //exit(1);
    }

    mysql_close(conn);

    char *database2 = "transaction";
    MYSQL *conn2 = mysql_init(NULL);


    if (!mysql_real_connect(conn2, server, sqluser, password, database2, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("conn2 connected \n");
        return -5;
    }

    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "CREATE TABLE %s (transaction varchar(30) DEFAULT NULL, comment varchar(128) DEFAULT NULL, date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, balance double NOT NULL) ENGINE=InnoDB;", formname);

    //printf("sqlcmd: %s", sqlcmd);

     if (mysql_query(conn2, sqlcmd)) {
        printf("errpoint ssi1\n");
        fprintf(stderr, "%s\n", mysql_error(conn2));
        printf("error in query account table!\n");
        mysql_close(conn2);
        return -2;
        //exit(1);
    }



    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd," INSERT INTO %s (comment, balance) VALUES ('Initialize', 0.0);", formname);


    //printf("sqlcmd: %s \n", sqlcmd);
    if (mysql_query(conn2, sqlcmd)) {
        printf("errpoint ssi2\n");
        fprintf(stderr, "%s\n", mysql_error(conn2));
        printf("error in query account table!\n");
        mysql_close(conn2);
        return -2;
        //exit(1);
    }

    //printf("success! \n");

    mysql_close(conn2);
    return 0;
}

int delete_account(char* account) {
    conn = mysql_init(NULL);
    char *database = "userinfo";

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("database connect error\n");
        return -5;
    }

    char sqlcmd[MAX_SQL_LEN] = {0};
    char formname[MAX_FORM_LEN] = {0};

    sprintf(sqlcmd, "SELECT transaction_form from account where account = '%s';", account);
    //printf("sqlcmd: %s \n", sqlcmd);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        return -5;
    }

    res = mysql_store_result(conn);

    if(!mysql_num_rows(res)) {
        printf("no account information\n");
        mysql_close(conn);
        return -1;
    } else if(mysql_num_rows(res) > 1) {
        printf("duplicate account in database! \n");
        mysql_close(conn);
        return -2;
    }

    if((row = mysql_fetch_row(res)) == NULL) {

        fprintf(stderr, "%s\n", mysql_error(conn));
        // exit(-1);
        mysql_close(conn);
        return -5;
    }

    strncpy(formname, row[0], strlen(row[0]));

    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "DELETE FROM account WHERE account = '%s';", account);

    //printf("sqlcmd: %s \n", sqlcmd);
    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error when deleting account! \n");
        mysql_close(conn);
        return -5;
    }

    mysql_close(conn);

    char *database2 = "transaction";
    MYSQL *conn2 = mysql_init(NULL);

    if (!mysql_real_connect(conn2, server, sqluser, password, database2, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -5;
    }

    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "DROP TABLE %s;", formname);

    if (mysql_query(conn2, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn2));
        printf("error when deleting transaction history! \n");
        mysql_close(conn2);
        return -5;
    }

    mysql_close(conn2);

    return 0;
}

// error : -1 teller name exsit!
//         -5 database operation error
int create_teller(char* teller, char* passwd) {
    conn = mysql_init(NULL);
    char *database = "userinfo";

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("database connect error\n");
        return -5;
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    // check if teller exists
    sprintf(sqlcmd, "SELECT * FROM teller WHERE username = '%s';", teller);
    //printf("sqlcmd %s", sqlcmd);
    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in query database! \n");
        mysql_close(conn);
        return -5;
    }

    res = mysql_store_result(conn);

    if(mysql_num_rows(res)) {
        printf("teller exsits! \n");
        mysql_close(conn);
        return -1;
    }
    mysql_free_result(res);

    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "INSERT INTO teller (username, password) VALUES ('%s', '%s');", teller, passwd);

    //printf("sqlcmd %s", sqlcmd);
    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in database insertion! \n");
        mysql_close(conn);
        return -5;
    }

    mysql_close(conn);
    return 0;
}

// error:
// -1 no such teller
// -2 duplicated teller error
// -5 database error
int delete_teller(char* teller) {
    conn = mysql_init(NULL);
    char *database = "userinfo";
    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("database connect error\n");
        return -5;
    }
    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT * FROM teller where username = '%s';", teller);
    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in query database! \n");
        mysql_close(conn);
        return -5;
    }

    res = mysql_store_result(conn);
    if(!mysql_num_rows(res)) {
        printf("no such teller! \n");
        mysql_close(conn);
        return -1;
    }

    if(mysql_num_rows(res) > 1) {
        printf("duplicated teller! \n");
        mysql_close(conn);
        return -2;
    }

    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "DELETE FROM teller where username = '%s';", teller);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in database deletion! \n");
        mysql_close(conn);
        return -5;
    }

    mysql_close(conn);
    return 0;
}


// error:
// -1 no such teller
// -2 duplicated teller error
// -5 database error
int change_teller_passwd(char* teller, char* passwd) {
    conn = mysql_init(NULL);
    char *database = "userinfo";
    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("database connect error\n");
        return -5;
    }
    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT * FROM teller where username = '%s';", teller);
    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in query database! \n");
        mysql_close(conn);
        return -5;
    }

    res = mysql_store_result(conn);
    if(!mysql_num_rows(res)) {
        printf("no such teller! \n");
        mysql_close(conn);
        return -1;
    }

    if(mysql_num_rows(res) > 1) {
        printf("duplicated teller! \n");
        mysql_close(conn);
        return -2;
    }

    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "UPDATE teller SET password = '%s' where username = '%s';", passwd, teller);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in database update! \n");
        mysql_close(conn);
        return -5;
    }

    mysql_close(conn);
    return 0;
}

int change_admin_passwd(char* adminname, char* passwd) {
    conn = mysql_init(NULL);
    char *database = "userinfo";
    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("database connect error\n");
        return -5;
    }
    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "UPDATE admini SET password = '%s' where username = '%s';", passwd, adminname);
    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error in database update! \n");
        mysql_close(conn);
        return -5;
    }

    return 0;
}

// action :
// 0 withdraw
// 1 deposit
// 2 query_balance
// 3 query_history
// 4 transfer

double common_call(char* name, char* account, char* recv_account, double amt, int query_row_num, int sock, int action) {
    // query table name of account
    conn = mysql_init(NULL);
    char *database = "userinfo";
    int UID = -1;

    if (!mysql_real_connect(conn, server, sqluser, password, database, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -5;
    }

    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd, "SELECT UID from customer where username = '%s';", name);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -5;
    }

    res = mysql_store_result(conn);

    // get user UID
    if((row = mysql_fetch_row(res)) == NULL) {

        printf(" Cannot fetch customer information! \n");
        return -1;
    }

    if(mysql_num_rows(res) != 1) {
        // mysql_error
    }

    UID = atoi(row[0]);

    // get transaction form name
    mysql_free_result(res);
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "SELECT transaction_form from account where UID = %d;", UID);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -2;
        //exit(1);
    }

    res = mysql_store_result(conn);
    if((row = mysql_fetch_row(res)) == NULL) {
        printf("cannot fetch account transaction history! \n");
        return -1;
    }

    char form_name[MAX_FORM_LEN] = {0};
    strncpy(form_name, row[0], strlen(row[0]));

    char *database2 = "transaction";
    MYSQL *conn2 = mysql_init(NULL);

    if (!mysql_real_connect(conn2, server, sqluser, password, database2, 0, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }


    // lock table first
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd,
            "lock tables %s write;",
            form_name);

    if (mysql_query(conn2, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn2));
        //exit(1);
        printf("query error %s \n", sqlcmd);
        return -5;
    }

    mysql_query(conn2, "show open tables where in_use > 0");
    res = mysql_store_result(conn2);

    while((row = mysql_fetch_row(res)) != NULL) {
        //printf("Database %s, Table %s, in use %s\n", row[0], row[1], row[2]);
    }

    // fetch last row in transaction table
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "SELECT * from %s ORDER BY date desc limit 0,1;", form_name);

    if (mysql_query(conn2, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        //exit(1);
        mysql_query(conn2, "unlock tables;");
        printf("query error 2 %s \n", sqlcmd);
        return -5;
    }

    mysql_free_result(res);
    res = mysql_store_result(conn2);

    double current_balance = 0.0;

    if((row = mysql_fetch_row(res)) == NULL) {
        printf(" Error occured when fetch in transaction history! \n");
        mysql_query(conn2, "unlock tables;");
        return -1;
    }

    // action :
    // 0 withdraw
    // 1 deposit
    // 2 query_balance
    // 3 query_history
    // 4 transfer
    double ret = -1;
    current_balance = atof(row[3]);
    //printf("action %d", action);
    switch(action) {
        case 0: {
            ret = withdraw_inner(current_balance, amt, conn2, form_name);
            break;
        }

        case 1: {
            ret = deposit_inner(current_balance, amt, conn2, form_name);
            break;
        }

        case 2: {
            ret = current_balance;
            break;
        }

        case 3: {
            ret = query_history_inner(conn2, form_name, query_row_num, sock);
            break;
        }

        case 4: {
            ret = transfer_inner(current_balance, amt, conn2, form_name, recv_account);
            break;
        }
    }

    if (mysql_query(conn2, "unlock tables;")) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        //exit(1);
        return -5;
    }

    mysql_close(conn2);
    mysql_free_result(res);
    mysql_close(conn);
    return ret;
}

int withdraw_inner(double current_balance, double amt, MYSQL* conn2, char* form_name) {
    char sqlcmd[MAX_SQL_LEN] = {0};
    printf("current balance %f, amount %f \n", current_balance, amt);

    if(current_balance >= amt) {
        double new_balance = current_balance - amt;
        sprintf(sqlcmd,
                "INSERT INTO %s (transaction, comment, balance) VALUES ('-%f', 'withdraw', %f);",
                form_name, amt, new_balance);
        printf("%s \n", sqlcmd);

        if (mysql_query(conn2, sqlcmd)) {
            fprintf(stderr, "%s\n", mysql_error(conn));
            //exit(1);
            return -5;
        }

        // double check
        return 0;
    }

    // balance < amt
    printf(" error ID 6\n");
    return -1;
}

int deposit_inner(double current_balance, double amt, MYSQL* conn2, char* form_name) {
    char sqlcmd[MAX_SQL_LEN] = {0};

    sprintf(sqlcmd,
            "INSERT INTO %s (transaction, comment, balance) VALUES ('+%f', 'deposit', %f);",
            form_name, amt, current_balance+amt);
    //printf("sqlcmd %s", sqlcmd);

    if (mysql_query(conn2, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_query(conn2, "unlock tables;");
        //exit(1);
        return -5;
    }
    // double check
    //
    return 0;
}


int query_history_inner(MYSQL *conn2, char* form_name, int query_row_num, int sock) {
    char sqlcmd[MAX_SQL_LEN] = {0};
    cJSON *root = cJSON_CreateObject();
    char* query_buf = NULL;

    sprintf(sqlcmd, "SELECT * from %s ORDER BY date limit 0, %d;", form_name, query_row_num);

    if (mysql_query(conn2, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));

        cJSON_AddNumberToObject(root, "result", 1.0);
        query_buf = cJSON_PrintBuffered(root, 1, 1);
        int send_len = write(sock, query_buf, strlen(query_buf));

        if(send_len < 0) {
            printf("send error! \n");
        }
        return -5;
    }

    cJSON_AddNumberToObject(root, "result", 0.0);

    mysql_free_result(res);
    res = mysql_store_result(conn2);

    int num_rows = mysql_num_rows(res);
    int num_fields = mysql_num_fields(res);
    int i = 0;
    char* query_msg = (char *) malloc(MAX_MSG_LEN * sizeof(char));
    char* msg_ptr = query_msg;

    // total package number
    cJSON_AddNumberToObject(root, "length", num_rows);
    cJSON* transaction_log = cJSON_CreateArray();
    cJSON* log_sequence = NULL;
    char blank[] = "    ";

    while((row = mysql_fetch_row(res))) {
        //memset(query_buf, 0, MAX_PACKAGE_SIZE);
        memset(query_msg, 0, MAX_MSG_LEN);
        msg_ptr = query_msg;

        for(i = 0; i < num_fields; i++) {

            /*
            printf("1 %s %d\n", row[i], num_fields);
            strncpy(msg_ptr, row[i], strlen(row[i]));
            printf("2 %s %s\n", msg_ptr, query_msg);
            msg_ptr += strlen(row[i]);
            strncpy(msg_ptr, blank, strlen(blank));
            printf("3 %s %s\n", msg_ptr, query_msg);
            msg_ptr += strlen(blank);
            */
            strcat(query_msg, row[i]);
            strcat(query_msg, blank);
            printf(" %s ", row[i]);
        }


        log_sequence = cJSON_CreateObject();

        cJSON_AddItemToObject(log_sequence, "log", cJSON_CreateString(query_msg));
        cJSON_AddItemToArray(transaction_log, log_sequence);
    }

    cJSON_AddItemToObject(root, "transaction log", transaction_log);

    query_buf = cJSON_PrintBuffered(root, 1, 1);

    int send_len = write(sock, query_buf, MAX_PACKAGE_SIZE);

    if(send_len < 0) {
        printf("send error! \n");
    }


    cJSON_Delete(root);

    return 0;

}


int transfer_inner(double current_balance, double amt, MYSQL* conn2, char* form_name, char* recv_account) {
    char sqlcmd[MAX_SQL_LEN] = {0};
    char recv_form[MAX_FORM_LEN] = {0};

    sprintf(sqlcmd, "SELECT transaction_form from account where account = '%s';", recv_account);

    if (mysql_query(conn, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        //exit(1);
        printf("error point 1\n");
        return -5;
    }

    mysql_free_result(res);
    res = mysql_store_result(conn);

    if((row = mysql_fetch_row(res)) != NULL) {
        strncpy(recv_form, row[0], strlen(row[0]));
        printf("form_name: %s\n", row[0]);
    } else {
        printf(" Error occurs in fetch transaction \n");
        return -1;
    }

    // lock receiver transaction table
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd,
            "lock tables %s write, %s write;",
            recv_form, form_name);


    if (mysql_query(conn2, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        //exit(1);
        printf("error point 2\n");
        return -5;
    }


    // get receiver balance
    double recv_balance = 0.0;
    memset(sqlcmd, 0, MAX_SQL_LEN);
    sprintf(sqlcmd, "SELECT * from %s ORDER BY date DESC  limit 0, 1;", recv_form);

    //printf("%s \n", sqlcmd);
    if (mysql_query(conn2, sqlcmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        printf("error point 3 \n");
        //exit(1);
        return -5;
    }

    mysql_free_result(res);
    res = mysql_store_result(conn2);

    if((row = mysql_fetch_row(res)) != NULL) {
        recv_balance = atof(row[3]);
    }

    // check then do transfer
    if(current_balance >= amt) {
        memset(sqlcmd, 0, MAX_SQL_LEN);
        sprintf(sqlcmd,
                "INSERT INTO %s (transaction, comment, balance) VALUES ('-%f', 'transfer to %s', %f);",
                form_name, amt, recv_form, current_balance-amt);

        //printf("%s \n", sqlcmd);



        if (mysql_query(conn2, sqlcmd)) {
            fprintf(stderr, "%s\n", mysql_error(conn2));
            printf("error point 4\n");
            //exit(1);
            return -5;
        }


        memset(sqlcmd, 0, MAX_SQL_LEN);
        sprintf(sqlcmd,
                "INSERT INTO %s (transaction, comment, balance) VALUES ('+%f', 'receiver from %s', %f);",
                recv_form, amt, form_name, recv_balance+amt);

        if (mysql_query(conn2, sqlcmd)) {
            fprintf(stderr, "%s\n", mysql_error(conn2));
            printf("error point 5\n");
            //exit(1);
            return -5;
        }
        return 0;
    } else {
        // sender balance is not enough
        return -1;
    }

    return -1;
}


