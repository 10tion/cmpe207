#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include <mysql/mysql.h>
#include "cJSON.h"

#include "opsql.h"
#include "customer.h"
#include "teller.h"
#include "admin.h"

#define QUEUE_LEN 32


#define MAX_BUF_SIZE 2048


void signal_child(int nouse);


int main(int argc, char **argv) {

    int PORTNUM = 0;

    PORTNUM = atoi(argv[1]);

    // TCP connect establish
    // build IPv4 structure
    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));

    server_info.sin_family = AF_INET;
    server_info.sin_port   = htons(PORTNUM);
    server_info.sin_addr.s_addr = INADDR_ANY;

    // create server TCP socket
    int server_soc = socket(PF_INET, SOCK_STREAM, 0);
    if(server_soc == -1){
        perror("Failed to create client socket, return");
        exit(-1);
    }

    // bind the socket
    int ret = bind(server_soc, (struct sockaddr *)&server_info, sizeof(server_info));
    if(ret != 0){
        printf("cannot bind to server:%d\n", PORTNUM);
        close(server_soc);
        exit(-1);
    }

    // back up original handler for SIGCHLD
    void(* old_handler)(int);
    // wait for child avoid zombie
    old_handler = signal(SIGCHLD, signal_child);

    while(1){

        printf("waiting for sending file, ");

        // listen socket
        ret = listen(server_soc, QUEUE_LEN);
        if(ret != 0){
            printf("cannot listen on %d\n", PORTNUM);
            close(server_soc);
            exit(-1);
        }

        printf("listen socket number is:%d\n", server_soc);

        // accept connection
        struct sockaddr_in client_info;
        memset(&client_info, 0, sizeof(client_info));
        socklen_t client_addr_len = sizeof(client_info);

        int accept_soc = accept(server_soc, (struct sockaddr*)&client_info, &client_addr_len);
        if(accept_soc < 0){
            printf("accept failed, %s\n", strerror(errno));
            close(server_soc);
            exit(-1);
        }

        printf("accept socket number is:%d, fork a new child process to handle\n", accept_soc);

        int child = fork();



        //this is child process
        if(child == 0) {

            char buf[MAX_BUF_SIZE] = {0};

            // receive CJSON login package
            // decipher package from TLS

            int buf_len = read(accept_soc, buf, MAX_BUF_SIZE);

            if(buf_len < 0) {
                // modify to write into log file
                printf("error to read from clients \n");
            }

            char *rcv_ptr = buf;

            cJSON *root = cJSON_Parse(rcv_ptr);



            if(root == NULL) printf("receiver NULL pkg \n");

            //char* action = ((cJSON *) cJSON_GetObjectItemCaseSensitive(root, "action"))->string;

            /*
            if(strcmp(action, "login")) {
                printf(" Not Login action \n");
                goto close_child;
            }
            */

            cJSON *who = cJSON_GetObjectItemCaseSensitive(root, "who");

            printf("string: %s \n", who->valuestring);


            printf("value of who: %f \n", who->valuedouble);

            if(cJSON_IsString(who)) {
                char* role = who->valuestring;



                if(!strcmp(role, "CUSTOMER")) {

                    customer_handler(accept_soc, root);
                    printf("customer return; \n");

                } else if(!strcmp(role, "TELLER")) {

                    teller_handler(accept_soc, root);

                } else if(!strcmp(role, "ADMIN")) {

                    admin_handler(accept_soc, root);

                } else {

                    printf(" invalid role of login \n");

                }
            }

close_child:
            close(accept_soc);
            exit(0);
        }

        // this is parent process
        close(accept_soc);
    }

    // restore original handler for SIGCHLD
    signal(SIGCHLD, old_handler);

    return 0;
}

void signal_child(int nouse){
    pid_t pid;
    int stat;

    while ((pid = waitpid(-1, &stat, WNOHANG)) > 0)
        printf("child_pid: %d dead\n", pid);
}
