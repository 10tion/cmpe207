#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <errno.h>
#include <mysql/mysql.h>

#include "cJSON.h"
#include "opsql.h"
#include "msg_typedef.h"

#define MAX_BUF_SIZE 1024
#define MAX_USER_LEN 16
#define MAX_ACCOUNT_LEN 32
#define MAX_MSG_LEN 128

static void msg_handler(int sock, char* errmsg, double res);

void admin_handler(int sock, cJSON *root) {

    cJSON *msg_recv;

    char buf[MAX_BUF_SIZE] = {0};

    cJSON *root_name = cJSON_GetObjectItemCaseSensitive(root, "admin name");
    cJSON *root_password = cJSON_GetObjectItemCaseSensitive(root, "admin password");

    char username[MAX_USER_LEN] = {0};
    char password[MAX_USER_LEN] = {0};

    char errmsg[MAX_MSG_LEN] = {0};


    if(cJSON_IsString(root_name)) {
        strncpy(username, (char *) root_name->valuestring, strlen((char *) root_name->valuestring));
    } else {
        printf(" invalid format of username in cJSON package \n");
        return;
    }

    if(cJSON_IsString(root_password)) {
        strncpy(password, (char *) root_password->valuestring, strlen((char *) root_password->valuestring));
    } else {
        printf(" invalid format of password in cJSON package \n");
        return;
    }


    int authen = authenticate_teller_admin (username, password, 0);
    memset(errmsg, 0, MAX_MSG_LEN);

    if(authen) {
        msg_handler(sock, "error !\n", 1.0);
        return;
    }

    msg_handler(sock, "success in login!\n", 0.0);

    int errcount = 0;

    while(1) {

        int buf_len = read(sock, buf, MAX_BUF_SIZE);

        if(buf_len <= 0) {

            if(errno != EINTR) {
                printf("CLient disconnected! Close!\n");
                return;
            }

            // modify to write into log file
            printf("error to read from clients \n");
            errcount++;

            if(errcount >= 10) return;
            continue;
        } else {
            errcount = 0;
        }

        char* buf_ptr = buf;

        msg_recv = cJSON_Parse(buf_ptr);

        //cJSON *msg_send = cJSON_CreateObject();
        //char buf_send[MAX_BUF_SIZE] = {0};

        if(msg_recv == NULL) {
            printf(" cannot parse received package \n");
            errcount++;

            if(errcount >= 10) {
                return;
            }
                continue;
        } else {
            errcount = 0;
        }


        cJSON *action = cJSON_GetObjectItemCaseSensitive(msg_recv, "action");

        int action_num = (int) action->valuedouble;
        printf("action: %d\n",(int) action->valuedouble);

        switch(action_num) {

            // create teller
            case 11: {
                char teller_name[MAX_ACCOUNT_LEN] = {0};
                char teller_passwd[MAX_ACCOUNT_LEN] = {0};

                cJSON *root_teller_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "teller name");
                cJSON *root_teller_password = cJSON_GetObjectItemCaseSensitive(msg_recv, "password");

                strncpy(teller_name,
                            (char *) root_teller_name->valuestring,
                            strlen((char *) root_teller_name->valuestring));

                strncpy(teller_passwd,
                            (char *) root_teller_password->valuestring,
                            strlen((char *) root_teller_password->valuestring));

                int ret = create_teller(teller_name, teller_passwd);

                switch(ret) {
                    case 0: {
                        msg_handler(sock, "teller created!", 0.0);
                        break;
                    }
                    case -1: {
                        msg_handler(sock, "teller exsits!", 1.0);
                        break;
                    }
                    case -5: {
                        msg_handler(sock, "Database error!", 1.0);
                        break;
                    }
                    default: {
                        msg_handler(sock, "unknow error!", 1.0);
                        break;
                    }
                }
                break;
            }

            // detele teller
            case 12: {
                char teller_name[MAX_ACCOUNT_LEN] = {0};

                cJSON *root_teller_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "teller name");

                strncpy(teller_name,
                            (char *) root_teller_name->valuestring,
                            strlen((char *) root_teller_name->valuestring));

                int ret = delete_teller(teller_name);

                switch(ret) {
                    case 0: {
                        msg_handler(sock, "teller deleted!", 0.0);
                        break;
                    }
                    case -1: {
                        msg_handler(sock, "no such teller!", 1.0);
                        break;
                    }
                    case -2: {
                        msg_handler(sock, "duplicate teller!", 1.0);
                        break;
                    }
                    case -5: {
                        msg_handler(sock, "Database error!", 1.0);
                        break;
                    }
                    default: {
                        msg_handler(sock, "unknow error!", 1.0);
                        break;
                    }
                }
                break;
            }

            // change teller passwd
            case 6: {
                char teller_name[MAX_ACCOUNT_LEN] = {0};
                char teller_passwd[MAX_ACCOUNT_LEN] = {0};

                cJSON *root_teller_name = cJSON_GetObjectItemCaseSensitive(msg_recv, "teller name");
                cJSON *root_teller_password = cJSON_GetObjectItemCaseSensitive(msg_recv, "new password");

                strncpy(teller_name,
                            (char *) root_teller_name->valuestring,
                            strlen((char *) root_teller_name->valuestring));

                strncpy(teller_passwd,
                            (char *) root_teller_password->valuestring,
                            strlen((char *) root_teller_password->valuestring));

                int ret = change_teller_passwd(teller_name, teller_passwd);

                switch(ret) {
                    case 0: {
                        msg_handler(sock, "password changed!", 0.0);
                        break;
                    }
                    case -1: {
                        msg_handler(sock, "no such teller!", 1.0);
                        break;
                    }
                    case -2: {
                        msg_handler(sock, "duplicate teller!", 1.0);
                        break;
                    }
                    case -5: {
                        msg_handler(sock, "Database error!", 1.0);
                        break;
                    }
                    default: {
                        msg_handler(sock, "unknow error!", 1.0);
                        break;
                    }
                }
                break;
            }

            //change self passwd
            case 13: {
                char admin_passwd[MAX_ACCOUNT_LEN] = {0};

                cJSON *root_admin_passwd = cJSON_GetObjectItemCaseSensitive(msg_recv, "new password");

                strncpy(admin_passwd,
                            (char *) root_admin_passwd->valuestring,
                            strlen((char *) root_admin_passwd->valuestring));

                int ret = change_admin_passwd(username, admin_passwd);

                if(!ret) msg_handler(sock, "password changed!", 0.0);
                else msg_handler(sock, "database error!", 1.0);

                break;
            }

            case 14: {
                         printf("admin log out! \n");
                         msg_handler(sock, "admin log out! \n", 0.0);
                         return;
                     }

            default: {
                         printf("invalid action\n");
                         msg_handler(sock, " invalid action! \n", 1.0);
                         break;
                     }
        }
    }

}


static void msg_handler(int sock, char* errmsg, double res) {
    char *buf = NULL;

    cJSON *err = cJSON_CreateObject();

    cJSON_AddNumberToObject(err, "result", res);
    cJSON_AddItemToObject(err, "message", cJSON_CreateString(errmsg));

    buf = cJSON_PrintBuffered(err, 1, 1);
    int send_len = write(sock, buf, MAX_BUF_SIZE);

    if(send_len < 0) {
        printf("send error! \n");
    }
    cJSON_Delete(err);
}
